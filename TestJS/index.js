"use strict"
/*
Посчитать количество ссылок на странице, вывести их содержимое
*/
/*
var a = document.querySelectorAll("a"); //создаем переменную а, в которую записываем массив всех ссылок страницы с помощью метода querySelectorAll, в "a" мы выбираем эл.(тег) который будет записывать
var numberLinks = 0; // создаем переменную для подсчета кол-ва ссылок

for (let i = 0; i < a.length; i++) { //бегаем по массиву а в котором все ссылки страницы
    var p = document.createElement("p"); // создаем переменную р, чтобы в нее записать новый тег <p>, который появится в html
    p.innerHTML = a[i].getAttribute("href"); // переменную р записываем в html, которая равна каждому значению из массива а, используя метов получения атрибута из этого значения (из тега <a> мы показываем только то, что в атрибуте href)
    document.querySelector("#links").appendChild(p); //обращаемся к элементу с селектором links и в конец этого элемента записываем переменную р с помощью метода appendChild
    numberLinks++;
}

var number = document.createElement("p"); // создаю переменную, в которой создам <p>
number.innerHTML = "Кол-во ссылок = " + numberLinks; // запишу эту перменную в html,она будет равна текст + кол-во ссылок
document.querySelector("#links").appendChild(number); // обращаемся к элементу с селектором links, чтобы записать number в конец этого элемента
*/
/*
Изменили содержимое последнего параграфа
var pp = document.querySelectorAll("p").length - 1; //
document.querySelectorAll("p")[pp].innerHTML = "Параграф 2"; //в боди добавляю p
*/

/*
Посчитать количество тегов “p” на странице которые имеют класс “phrase” - вывести их содержимое
*/
/*
var masP = document.querySelectorAll(".phrase");
var numberParagraph = 0;

for (let i = 0; i < masP.length; i++) {
    var par = document.createElement("p");
    par.innerHTML = masP[i].innerText;
    document.querySelector("#paragraph").appendChild(par);
    numberParagraph++;
}

var numberP = document.createElement("p");
numberP.innerHTML = "Кол-во параграфов = " + numberParagraph;
document.querySelector("#paragraph").appendChild(numberP);
*/
/*
В задании из пятого урока, взять массив со студентами и вывести их на страницу согласно сверстанной HTML-структуре,
 рядом с каждым студентом вывести крестик - по нажатию на который студент будет удален (удаляется как со страницы, так и с объекта),
 если был удален последний студент написать текстовое сообщение (“Студенты не найдены”)
*/

let students = {
    st1: {
        name: "Vanya",
        estimate: 4.2,
        course: 2,
        active: true,
    },
    st2: {
        name: "Alex",
        estimate: 5,
        course: 1,
        active: false,
    },
    st3: {
        name: "Vova",
        estimate: 2,
        course: 2,
        active: false,
    },
    st4: {
        name: "Victoria",
        estimate: 3.2,
        course: 5,
        active: true,
    },
    st5: {
        name: "Viktor",
        estimate: 4.2,
        course: 4,
        active: true,
    },
    st6: {
        name: "Pavel",
        estimate: 3.3,
        course: 1,
        active: false,
    },
    st7: {
        name: "Bogdan",
        estimate: 4.4,
        course: 2,
        active: true,
    },
    st8: {
        name: "Julia",
        estimate: 3.1,
        course: 3,
        active: false,
    },
    st9: {
        name: "Artem",
        estimate: 4.9,
        course: 2,
        active: true,
    },
    st10: {
        name: "Georgiy",
        estimate: 2.5,
        course: 5,
        active: false,
    },
    st11: {
        name: "Andrey",
        estimate: 3.1,
        course: 5,
        active: true,
    },
    st12: {
        name: "Kate",
        estimate: 3.6,
        course: 3,
        active: false,
    },
    st13: {
        name: "Natali",
        estimate: 4.8,
        course: 1,
        active: true,
    },
    st14: {
        name: "Svetlana",
        estimate: 4.9,
        course: 3,
        active: false,
    },
    st15: {
        name: "Yra",
        estimate: 1.5,
        course: 2,
        active: false,
    },
    st16: {
        name: "Leo",
        estimate: 3,
        course: 4,
        active: true,
    },
    st17: {
        name: "Sergey",
        estimate: 3.9,
        course: 4,
        active: false,
    },
    st18: {
        name: "Maria",
        estimate: 4.3,
        course: 5,
        active: true,
    },
    st19: {
        name: "Alena",
        estimate: 4.4,
        course: 1,
        active: false,
    },
    st20: {
        name: "Olga",
        estimate: 4.5,
        course: 2,
        active: true,
    },
}


var table = document.createElement("table");
table.innerHTML = "<tr><th>jhkhj<th>jhkhj<th>jhkhj<th>jhkhj";
document.querySelectorAll("div")[0].append(table);



for (let i in students) {

    var str = document.createElement("tr");
    var yacheyka = document.createElement("td");
    yacheyka.innerHTML = students[i].name;
    var yacheyka2 = document.createElement("td");
    yacheyka2.innerHTML = students[i].estimate;
    var yacheyka3 = document.createElement("td");
    yacheyka3.innerHTML = students[i].course;
    str.append(yacheyka,yacheyka2,yacheyka3);
    document.querySelector("tbody").appendChild(str);
}
