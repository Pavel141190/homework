var firstNumber = 2000; //Первое число
var secondNumber = 1500; // Второе число
var leastNumber = firstNumber; //Наименьшее число из двух заданных (Для того чтобы начать проверять делители из наименьшего). Нет смысла проверять числа которые больше чем наименьшее из 2 заданных чисел
if (firstNumber > secondNumber) {
  leastNumber = secondNumber; //Если первое число больше чем второе, то присвоить наименьшее для второго числа
}
for (i = leastNumber; i > 0; i--) { //Цикл который проверяет делится ли первое и второе число без остатка на i начиная с наименьшего числа
  if (firstNumber % i == 0 && secondNumber % i == 0) {
    console.log(i);
    break;
  }
}
