// Урок 5. Задание 1
// Задан двумерный массив - объединить каждый внутренний массив с верхнем массивом - только по уникальным значениям.
// Например [1,2,4[8,4,12,],[13,29,11],[0,5,3,11],5,6,7,[3,8,21],3], получаем в результате: [1,2,4,8,12,13,29,11,0,5,3,11,6,7,21]



var mas = [1, 111, 2, [1, 2, 3, 4], 1, 2, 111, 111, 111, 3, [1, 6, 5, ], 6, 5, 7, [8, 9, 10, 111],
  [30, 31, 32, 4]
];
//1Й ВАРИАНТ. ЧЕРЕЗ РЕКУРСИЮ
var masUnikal = []; // Массив для записи уникальных чисел
function allElements(mas) { // Рекурсивная функция для получения массива уникальных чисел
  for (let i in mas) { // Цикл по всему заданому массиву
    if (typeof(mas[i]) == "object") { //Если элемент массива имеет тип "object"
      allElements(mas[i]); //  Запускаем функцию для этого массива
    } else { //Если элемент массива не является элементом с типом "object"
      var sovpadeniya = 0; // Переменная для определения есть ли в masUnikal уже такое число
      for (var j = 0; j < masUnikal.length; j++) { // Цикл который сравнивает элемент исходного массива с элементами уникального массива
        if (mas[i] == masUnikal[j]) { // Если есть совпадения чисел, то sovpadeniya увеличиваем на 1
          sovpadeniya++;
        }
      }
      if (sovpadeniya == 0) { // Если не было ни одного совпадения чисел, значит sovpadeniya будет равна 0 и мы можем записать наше число в уникальный массив
        masUnikal.push(mas[i]); // Добавляем в уникальный массив просматриваемый элемент в исходном массиве
      }
    }
  }
  return masUnikal; // возвращаем массив с уникальными числами
}
console.log(allElements(mas));

// 2Й ВАРИАНТ. ПРОСТО ЦИКЛ ВНУТРИ ЦИКЛА. КОД ПОЛУЧАЕТСЯ СЛОЖНЕЕ
var masUnikal2 = []; // Массив для записи уникальных чисел
function allElements2(mas) { // Функция для двумерного массива для получения массива уникальных чисел
  for (let i = 0; i < mas.length; i++) { // // Цикл по всему заданому массиву
    if (typeof(mas[i]) == "object") { //Если элемент массива имеет тип "object"
      for (let j = 0; j < mas[i].length; j++) { //Запускаем цикл который пройдется по внутреннему массиву
        var sovpaden = 0; // Переменная для определения есть ли совпадения чисел в этом массиве и уникальном массиве
        for (let t = 0; t < masUnikal2.length; t++) { // Цикл который сравнивает все эелементы внутреннего массива с элементами уникального
          if (mas[i][j] == masUnikal2[t]) { //Если есть совпадения
            sovpaden++; // Увеличиваем переменную sovpaden на 1
          }
        }
        if (sovpaden == 0) { // Если не было ни одного совпадения чисел, значит sovpaden будет равна 0 и мы можем записать наше число в уникальный массив
          masUnikal2.push(mas[i][j]);
        }
      }
    } else { //Если элемент исходного массива mas не является типом object
      var sovpaden = 0; // Переменная для определения есть ли совпадения чисел в исходном массиве mas и уникальном массиве
      for (let t = 0; t < masUnikal2.length; t++) { // Цикл который проходится по уникальному массиву и сравнивает число исходного массива с уникальным
        if (mas[i] == masUnikal2[t]) { // Если есть совпадения чисел
          sovpaden++; //Увеличиваем переменную sovpaden на 1
        }
      }
      if (sovpaden == 0) { // Если sovpaden = 0 занчит не было совпадений и мы можем писать число исходного массива в уникальный
        masUnikal2.push(mas[i]);
      }
    }
  }
return masUnikal2; // Возвращаем наш уникальный массив
}
console.log(allElements2(mas));
