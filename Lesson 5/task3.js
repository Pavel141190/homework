// Урок 5. Задание 3
// Задан массив объектов студентов вида [{name: “Ivan”, estimate: 4, course: 1, active: true},
//   {name: “Ivan”, estimate: 3, course: 1, active: true},{name: “Ivan”, estimate: 2, course: 4, active: false},
//   {name: “Ivan”, estimate: 5, course: 2, active: true}] - заполнить его более большим количеством студентов.
//   Написать функцию которая возвращает: среднюю оценку студентов в разрезе каждого курса: {1: 3.2, 2: 3.5, 3: 4.5, 4: 3, 5: 4.5}
//   с учетом только тех студентов которые активны. Посчитать количество неактивных студентов в разрезе каждого курса и общее количество неактивных студентов.

var masStudents = [{
    name: "Pavel",
    estimate: 4,
    course: 1,
    active: true
  },
  {
    name: "Igor",
    estimate: 4.2,
    course: 2,
    active: true
  },
  {
    name: "Evheniy",
    estimate: 4.4,
    course: 3,
    active: true
  },
  {
    name: "Evheniy",
    estimate: 4.4,
    course: 3,
    active: false
  },
  {
    name: "Inna",
    estimate: 4.1,
    course: 4,
    active: true
  },
  {
    name: "Natalia",
    estimate: 4.6,
    course: 1,
    active: true
  },
  {
    name: "Sergey",
    estimate: 4.2,
    course: 3,
    active: false
  },
  {
    name: "Sergey",
    estimate: 4.2,
    course: 1,
    active: false
  },
  {
    name: "Sergey",
    estimate: 4.2,
    course: 2,
    active: false
  },
  {
    name: "Evheniy",
    estimate: 4.4,
    course: 5,
    active: false
  },
  {
    name: "Sergey",
    estimate: 4.2,
    course: 1,
    active: false
  },
  {
    name: "Sergey",
    estimate: 4.2,
    course: 4,
    active: false
  },
  {
    name: "Alex",
    estimate: 4.8,
    course: 2,
    active: true
  },
  {
    name: "Maxim",
    estimate: 5,
    course: 1,
    active: true
  },
  {
    name: "Alexey",
    estimate: 4.6,
    course: 4,
    active: true
  },
  {
    name: "Anna",
    estimate: 4.9,
    course: 5,
    active: true
  },
  {
    name: "Artem",
    estimate: 4.4,
    course: 4,
    active: true
  },
  {
    name: "Ivan",
    estimate: 4.3,
    course: 5,
    active: true
  },
  {
    name: "Albert",
    estimate: 4.1,
    course: 5,
    active: true
  },
];

function analitika(active) {
  let res; // Переменная для вывода результата функции. Вывод объекта со средники оценками или объекта с кол-ом неактивных студентов
  let objAnalitik = {}; // Объект для записи средник оценок
  let objNeaktivStud = {}; // Объект для записи информации по неактивным студентам
  let offStudent = 0; // Переменная для подсчета неактивных студентом на всех курсах
  for (let i = 0; i < 5; i++) { //Цикл который запускает 5 итераций для расчета на каждом курсе
    let offStudentCourse = 0; //Переменная для подсчета неактивных студентом на каждом курсе (внутри цикла для обнуления расчета для каждого курса)
    let srednEstim = 0; //Переменная для расчета средней оценки курса (внутри цикла для обнуления расчета для каждого курса)
    let kolStudents = 0; //Переменная для расчета кол-ва студентов на курсе для вычисления в дальнейшем средней оценки (внутри цикла для обнуления расчета для каждого курса)
    for (let j in masStudents) { // Цикл который перебирает все объекты в нашем массиве студентов
      if (masStudents[j].active == true && masStudents[j].course == i + 1 && active == true) { //Если статус студента активен, если студент на курсе который в данный момент проверяет цикл и если функция запущена для получения true данных
        srednEstim += masStudents[j].estimate; // Считаем общую сумму баллов каждого курса
        kolStudents++; //Считаем сколько студентов на каждом курсе
      } else if (active == false && masStudents[j].active == false && masStudents[j].course == i + 1) { // Если функия запущена с параметром false, статус студента не активен и его курс соответствует курсу который проверяется в цикле
        offStudent++; // Считаем сколько всего неактивных студентов
        offStudentCourse++; // Считаем сколько неактивных студентов на каждом курсе
      }
    }
    if (active == true) { // После того как цикл с j прошелся по всем студентам в массиве задаем if который работает если функция запущена с true
      srednEstim = Math.floor(srednEstim / kolStudents * 100) / 100; // Расчет средней оценки для курса (окончание цикла для каждого курса. *100/100 для округления до 2 знаков)
      objAnalitik[i + 1] = srednEstim; // Запись в объект средней оценки для каждого курса начиная с 1
      res = objAnalitik; // Если функция запущена с true то в return у нас выведется объект со средними оценками так как мы выводим в конце функции res
    } else if (active == false) { // После того как цикл с j прошелся по всем студентам в массиве задаем if который работает если функция запущена с false
      objNeaktivStud[i + 1] = offStudentCourse; // Запись в объект кол-ва неактивных студентов по каждому курсу
      res = objNeaktivStud; // Если функция запущена с false то в return у нас выведется объект с неактивными студентами так как мы выводим в конце функции res
    }
  }
  objNeaktivStud.neaktivStud = offStudent; //В конце функции дописываем в объект с неактивными студентами кол-во всех неактивных студентов с ключом neaktivStud
  return res; // Вывод функции. Объект со средними оценками в случае запуска функции с true или объект с неакт. студентами в случае запуска с false
}
console.log(analitika(true)); //Задаем статус студента. Если тру то выдаст средний бал по каждому курсу только активных студентов.
// Если фолс то кол-во неактивных по каждому курсу и общее их кол-во
