// Урок 4. Задача 5
// Написать рекурсивную функцию которая выводит абсолютно все элементы ассоциативного массива (объекта) - любого уровня вложенности

var mas = [
  [
    [1, 2, 3],
    [4, 5, 6, 7],
    [8, 9, 10, 11, 12]
  ],
  [
    [13, 14, 15],
    [16, 17, 18, 19],
    [20, 21, 22, 23, 24]
  ],
  [
    [1, 2, 3],
    [1, 2, 3, 4],
    [1, 2, 3, 4, 5],
    {
      name: "Pavel",
      secondName: "Chernobay"
    }
  ]
];

function vivodVsehElementov(mas) {
  for (let i in mas) {
    if (typeof(mas[i]) === "object") { // НЕ ИСПОЛЬЗОВАЛ ARRAY ПОТОМУ ЧТО ОПРЕДЕЛЯЕТ МАССИВ И ОБЪЕКТ КАК OBJECT
      vivodVsehElementov(mas[i]);
    } else {
      console.log(mas[i]);
    }
  }
}
vivodVsehElementov(mas);
