"use strict";

// Урок 4. Задача 1
// Использовать функцию из предыдущего задания чтобы получить массив из нужного количества значений.
// Найти процентное соотношение отрицательных, положительных и нулевых элементов массива.


function masGeneration(dlinaMas, minNumber, maxNumber) {
  if (dlinaMas > (maxNumber + 1 - minNumber)) { // В СЛУЧАЕ ЕСЛИ КОД ВЫПОЛНИТЬ НЕВОЗМОЖНО ТАК КАК ЗАДАНО ЭЛЕМЕНТОВ БОЛЬШЕ ЧЕМ ВОЗМОЖНО ВЫДАТЬ УНИКАЛЬНЫХ
    return mas = [];
  }
  var mas = [Math.floor(Math.random() * (maxNumber + 1 - minNumber) + minNumber)]; // 1ОЕ ЧИСЛО В МАССИВ ПРОСТО ЗАПИСЫВАЕМ СЛУЧАЙНЫМ ОБРАЗОМ
  for (var i = 1; i < dlinaMas; i++) {


    (function proverkaNaSovpadenie() { //САМОЗАПУСКАЮЩАЯСЯ ФУНКЦИЯ
      var test = 0; //ПЕРЕМЕННАЯ ДЛЯ ПОНМАНИЯ ТОГО ПОПАДАЛИСЬ ЛИ У НАС СОВПАДЕНИЯ ПРИ ПРОВЕРКЕ ПОЛУЧЕННОГО МАССИВА НА ДАННЫЙ МОМЕНТ
      var randomNumber = Math.floor(Math.random() * (maxNumber + 1 - minNumber) + minNumber);
      for (var j = 0; j < mas.length; j++) { //ПРОХОДИМСЯ ПО СУЩЕСТВУЮЩЕМУ МАССИВУ НА ДАННЫЙ МОМЕНТ И ПРОВЕРЯЕМ НЕ СОВПАДАЕТ ЛИ ПОЛУЧЕННОЕ ЧИСЛО С ТЕМИ КОТОРЫЕ УЖЕ ТАМ

        if (mas[j] == randomNumber) {
          test++; //ЕСЛИ У НАС СГЕНЕРИРОВАННОЕ ЧИСЛО НЕ РАВНЯЕТСЯ УЖЕ ЗАПИСАННЫМ ЭЛЕМЕНТАМ В МАССИВЕ ТО УВЕЛИЧИВАЕМ ЕЁ НА 1

        }
      }
      if (test == 0) {
        mas[i] = randomNumber;
      } else {

        proverkaNaSovpadenie(); // ПОПАДАЕМ СЮДА В СЛУЧАЕ ЕСЛИ ХОТЬ 1 ЧИСЛО ИЗ МАССИВА СОВПАЛО С ПОЛУЧЕННЫМ. ПЕРЕЗАПУСКАЕМ ФУНКЦИЮ КОТОРАЯ СГЕНЕРИРУЕТ НОВОЕ ЧИСЛО И ПРОВЕРИТ ЕГО
      }
    }());
  }
  return mas;
}


function pavel(dlinaMas, minNumber, maxNumber) {
  var pologitelnie = 0;
  var otricatel = 0;
  var nulevie = 0;
  var mas2 = masGeneration(dlinaMas, minNumber, maxNumber);
  for (var i = 0; i < mas2.length; i++) { //В ЭТОМ ЦИКЛЕ ПОЛУЧАЕМ КОЛИЧЕСТВО ПОЛОЖИТЕЛЬНЫХ, ОТРИЦАТЕЛЬНЫХ И НУЛЕВЫХ ЭЛЕМЕТОВ
    if (mas2[i] > 0) {
      pologitelnie++;
    } else if (mas2[i] == 0) {
      nulevie++;
    } else if (mas2[i] < 0) {
      otricatel++;
    }
  }
  var procentPolog = Math.round(pologitelnie / dlinaMas * 100 * 100) / 100; //ИСПОЛЬЗУЮ УМНОЖЕНИЕ НА 100 ЗАТЕМ ОКРУГЛЕНИЕ И ПОТОМ СНОВА ДЕЛЕНИЕ НА 100 ДЛЯ ПОЛУЧЕНИЯ 2 ЗНАКОВ ПОСЛЕ ЗАПЯТОЙ
  var procentOtrica = Math.round(otricatel / dlinaMas * 100 * 100) / 100;
  var procentNulle = Math.round((100 - procentPolog - procentOtrica) * 100) / 100;
  console.log(procentPolog + "% Положительных чисел");
  console.log(procentOtrica + "% Отрицательных чисел");
  console.log(procentNulle + "% Нулевых чисел");
  return mas2;

}
console.log(pavel(12, -9, 9));
