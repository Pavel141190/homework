"use strict";

// Урок 4. Задача 1
// Все предыдущий задания на циклы - написать с помощью циклов for in и/или for of

// Урок 2. Задача 1
// Переменная содержит в себе строку. Вывести строку в обратном порядке.
var a = "ТСКЕТ ЬТУНРЕВЕРЕП";
var fullName = [];
for (let i in a) {
  fullName = a[i] + fullName; // fullName СТОИТ В КОНЦЕ УРАВНЕНИЯ ЧТОБЫ ПОЛУЧЕННЫЙ СИМВОЛ В ЦИКЛЕ ДОБАВИТЬ В НАЧАЛО ТЕКСТА, А НЕ В КОНЕЦ
}
console.log(fullName);


// Урок 2. Задача 4
// Найти сумму цифр числа которые кратны двум
var number = 1212121244118;
var result = 0;
var numberString = number + "";

for (let i in numberString) {
  if (numberString[i] % 2 == 0) {
    var preob = parseInt(numberString[i]);
    result += preob;
  }
}
console.log("Сумма цифр кратных 2 = " + result);

// Урок 3. Задача 3
// Написать функцию, которая транспонирует матрицу
var matrix = [
  [1, 2, 3, 1],
  [4, 5, 6, 4],
  [7, 8, 9, 7]
];
var reverseMas = [];
for (let i in matrix) {

  for (let j in matrix[i]) {
    if (i == 0) {
      reverseMas[j] = [];
    }

    reverseMas[j][i] = matrix[i][j];
  }

}
console.log("Перевернутый массив")
console.log(reverseMas);

// Урок 3. Задача 4
// Написать функцию, которая складывает две матрицы

var matrix = [
  [1, 2, 3, 1],
  [4, 5, 6, 4],
  [7, 8, 9, 9]
];
var matrix2 = [
  [1, 2, 3, 1],
  [4, 5, 6, 4],
  [7, 8, 9, 9]
];
var matrixSum = [];

for (let i in matrix) {
  matrixSum[i] = [];
  for (let j in matrix[i]) {
    matrixSum[i][j] = matrix[i][j] + matrix2[i][j];
  }
}
console.log("Сумма 2 матриц");
console.log(matrixSum);

// Урок 3. Задача 5
// Найти номер столбца двумерного массива сумма которого является максимальной (аналогично для строки)
var summaStrinMax = 0;
var numberString = 0;
var matrix = [
  [1, 2, 3, 1],
  [4, 5, 6, 4],
  [10, 10, 10, 10]
];
for (let i in matrix) {
  var summaString = 0;
  for (let j in matrix[i]) {
    summaString += matrix[i][j];
  }
  if (summaStrinMax < summaString) {
    summaStrinMax = summaString;
  }
}

console.log("Максимальная сумма строки = " + summaStrinMax);
