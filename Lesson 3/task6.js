"use strict";
// Урок 3. Задача 6
// Удалить из массива все столбцы которые не имеют ни одного нулевого элемента и сумма которых положительна - оформить в виде функции


// ГЕНЕРИРОВАНИЕ МАССИВА РАЗМЕРОМ gorizontal НА vertikal с максимально возможным заданным числом в массиве
function ganerateMas(gorizontal, vertikal, minRandomNumber, maxRandomNumber) {
  var mas = [];
  for (var i = 0; i < vertikal; i++) {
    mas[i] = [];
    for (var j = 0; j < gorizontal; j++) {
      mas[i][j] = Math.floor(Math.random() * (maxRandomNumber + 1 - minRandomNumber) + minRandomNumber);
    }
  }
  return mas;
}

function remove(gorizontal, vertikal, minRandomNumber, maxRandomNumber) {
  var resultMas = ganerateMas(gorizontal, vertikal, minRandomNumber, maxRandomNumber);
  for (var i = gorizontal - 1; i > -1; i--) {
    var summaStolb = 0;
    var kolNullElementov = 0;
    for (var j = 0; j < vertikal; j++) {
      summaStolb += resultMas[j][i];
      if (resultMas[j][i] == 0) { //ЕСЛИ В СТОЛБЦЕ НЕТ ЭЛЕМЕНТОВ РАВНЫХ 0 СТОЛБЕЦ УДАЛЯЕТСЯ (НАЧИНАЕТ С КОНЦА)
        kolNullElementov++;
      }
    }
    if (kolNullElementov == 0 && summaStolb > 0) {
      for (var t = 0; t < vertikal; t++) {
        resultMas[t].splice(i, 1);
      }
    }
  }
  return (resultMas);
}
console.log("Всё что осталось от случайно полученного массива");
console.log(remove(10, 3, -5, 5)); //В ПАРАМЕТРЫ ФУНКЦИИ ВВОДИМ РАЗМЕР МАТРИЦЫ ПО ГОРИЗОНТАЛИ И ВЕРТИКАЛИ, МИНИМАЛЬНОЕ И МАКСИМАЛЬНОЕ ЗНАЧЕНИЕ МАТРИЦЫ
