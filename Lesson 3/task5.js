"use strict";
// Урок 3. Задача 5
// Найти номер столбца двумерного массива сумма которого является максимальной (аналогично для строки)

// ГЕНЕРИРОВАНИЕ МАССИВА РАЗМЕРОМ gorizontal НА vertikal с максимально возможным заданным числом в массиве

function ganerateMas(gorizontal, vertikal, maxRandomNumber) {
  var mas = [];
  for (var i = 0; i < vertikal; i++) {
    mas[i] = [];
    for (var j = 0; j < gorizontal; j++) {
      mas[i][j] = Math.floor(Math.random() * maxRandomNumber + 1);
    }
  }
  console.log("Массив " + gorizontal + "x" + vertikal + ". Наполнен случайными цифрами от 0 до " + maxRandomNumber);
  console.log(mas);
  return mas;
}

// ФУНКЦИЯ ДЛЯ ОПРЕДЕЛЕНИЯ НОМЕРА СТОЛБЦА С МАКСИМАЛЬНОЙ СУММОЙ

function maxStolbec(gorizontal, vertikal, maxRandomNumber) {
  var mas = ganerateMas(gorizontal, vertikal, maxRandomNumber);
  var summaStolbMax = 0;
  var numberStolbec = 0;
  for (var i = 0; i < gorizontal; i++) {
    var summaStolb = 0;
    for (var j = 0; j < vertikal; j++) {
      summaStolb += mas[j][i];
    }
    if (summaStolbMax < summaStolb) {
      summaStolbMax = summaStolb;
      numberStolbec = i;
    }
  }
  console.log("Столбец № " + (numberStolbec) + " имеет максимальную сумму равную " + summaStolbMax);

  return (numberStolbec);
}
console.log(maxStolbec(5, 3, 5));

// ФУНКЦИЯ ДЛЯ ОПРЕДЕЛЕНИЯ НОМЕРА СТРОКИ С МАКСИМАЛЬНОЙ СУММОЙ

function maxStroka(gorizontal, vertikal, maxRandomNumber) {
  var mas = ganerateMas(gorizontal, vertikal, maxRandomNumber);
  var summaStringMax = 0;
  var numberString = 0;
  for (var i = 0; i < vertikal; i++) {
    var summaString = 0;
    for (var j = 0; j < gorizontal; j++) {
      summaString += mas[i][j];
    }
    if (summaStringMax < summaString) {
      summaStringMax = summaString;
      numberString = i;
    }
  }
  console.log("Строка № " + (numberString) + " имеет максимальную сумму равную " + summaStringMax);
  return (numberString);
}
console.log(maxStroka(3, 5, 5));
