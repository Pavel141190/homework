"use strict";
// Урок 3. Задача 1
// Все скрипты которые писали в рамках первого и второго задания - оформить в виде функций
// ======================================================================================================================================

// Урок 1. Задача 1.1.
// Переменная хранит в себе значение от 0 до 9. Написать скрипт который будет выводить слово “один”, если переменная хранит значение 1.
// Выводить слово “два” - если переменная хранит значение 2, и т.д. для всех цифр от 0 до 9. Реализовать двумя способами.
function vivodChisla(a) {
  if (a === 0) {
    return "Ноль";
  } else if (a === 1) {
    return "Один";
  } else if (a === 2) {
    return "Два";
  } else if (a === 3) {
    return "Три";
  } else if (a === 4) {
    return "Четыре";
  } else if (a === 5) {
    return "Пять";
  } else if (a === 6) {
    return "Шесть";
  } else if (a === 7) {
    return "Семь";
  } else if (a === 8) {
    return "Восемь";
  } else if (a === 9) {
    return "Девять";
  } else {
    return "Число не попадает в интервал от 0 до 9";
  }
}
console.log(vivodChisla(1));

function vivodChisla2(a) {
  switch (a) {
    case 0:
      return "Ноль";
      break;
    case 1:
      return "Один";
      break;
    case 2:
      return "Два";
      break;
    case 3:
      return "Три";
      break;
    case 4:
      return "Четыре";
      break;
    case 5:
      return "Пять";
      break;
    case 6:
      return "Шесть";
      break;
    case 7:
      return "Семь";
      break;
    case 8:
      return "Восемь";
      break;
    case 9:
      return "Девять";
      break;
    default:
      return "Число не попадает в интервал от 0 до 9";
  }
}
console.log(vivodChisla2(9));

// Урок 1. Задача 1.2.
// Переменная хранит в себе значение, напишите скрипт которое выводит информацию о том, что число является нулевым либо положительным либо отрицательным.
function znakChisla(a) {
  if (a < 0) {
    return "Число отрицательное";
  } else if (a > 0) {
    return "Число положительное";
  } else if (a === 0) {
    return "Переменная равна нулю";
  } else {
    return "Проверьте переменную, что-то не так";
  }
}
console.log(znakChisla(-2));

// Урок 1. Задача 2
// Переменная хранит в себе единицу измерения одно из возможных значений (Byte, KB, MB, GB), Вторая переменная хранит в себе целое число.
// В зависимости от того какая единица измерения написать скрипт, который выводит количество байт.
// Для вычисления принимает счет что в каждой последующей единицы измерения хранится 1024 единиц более меньшего измерения.

function razmernost(number, razmernost) {
  if (razmernost == "Byte") {
    return (number + " Байт. Преобразование не требуется");
  } else if (razmernost == "KB") {
    return (number + " Килобайт" + " = " + number * 1024 + " Байт");
  } else if (razmernost == "MB") {
    return (number + " Мегабайт" + " = " + number * 1024 * 1024 + " Байт");
  } else if (razmernost == "GB") {
    return (number + " Гигабайт" + " = " + number * 1024 * 1024 * 1024 + " Байт");

  } else {
    return "Не верно введена размерность"
  }
}
console.log(razmernost(1, "KB"));

// Урок 1. Задача 3
// Переменная хранит процент кредита, вторая переменная хранит объем тела кредита, третья переменная хранит длительность кредитного договора в годах. Написать скрипт который вычислит:
// Сколько процентов заплатит клиент за все время
// Сколько процентов заплатит клиент за один календарный год
// Какое общее количество денежных средств клиента банка выплатит за все года
function kredit(summaKredita, procentKredita, srok, analiz) {
  var fullTime = (summaKredita * procentKredita / 100 * srok);
  var oneYear = (summaKredita * procentKredita / 100);
  var fullMoney = summaKredita + (summaKredita * procentKredita / 100 * srok);
  if (analiz == "Все время") {
    return ("За всё время клиент заплатит " + fullTime + " грн кредита (проценты)");
  } else if (analiz == "Один год") {
    return ("За 1 календарный год клиент заплатит " + oneYear + " грн кредита (проценты)");
  } else if (analiz == "Все деньги") {
    return ("За всё время клиент выплатит банку " + fullMoney + " грн кредита");
  }
}
console.log(kredit(1000, 10, 5, "Все время"));

// Урок 2. Задача 1
// Переменная содержит в себе строку. Вывести строку в обратном порядке.

function reverse(string) {
  var fullName = "";
  for (var i = string.length - 1; i > -1; i--) {
    fullName += string[i];
  }
  return fullName;
}
console.log(reverse("ЬТУНРЕВЕРЕП"));

// Урок 2. Задача 2
// Переменная содержит в себе число. Написать скрипт который посчитает факториал этого числа.

function factorialChisla(number) {
  var fuctorial = 1;
  for (var i = 1; i < number + 1; i++) {
    fuctorial = fuctorial * i;
  }

  return fuctorial;
}
console.log("Факториал заданного числа = " + factorialChisla(5));

// Урок 2. Задача 3
// Дано число - вывести первые N делителей этого числа нацело.
function deliteli(number, n) {
  var kolDeliteley = 0;                         // Для того чтобы начало считать
  var delitelMas = [];                          //Массив в который буду записываться все наши делители
  for (var i = 1; n > kolDeliteley; i++) {
    if (number % i === 0) {
      kolDeliteley++;
      delitelMas.push(i);
    } else
    if (i > number) { //В случае если делителей меньше чем мы задали
      console.log("Количество делителей меньше чем задано");
      break;
    }
  }
  return delitelMas;
}
console.log("Перечень делителей: " + deliteli(36, 4));

// Урок 2. Задача 4
// Найти сумму цифр числа которые кратны двум
function summaKratn (number) {
  var result = 0;
  var numberString = number + "";
  for (var i = 0; i < numberString.length; i++) {
    if (numberString[i] % 2 == 0) {
      var preob = parseInt(numberString[i]);
      result += preob;
    }
  }
  return result;
}
console.log("Сумма цифр числа кратных 2 = " + summaKratn(22481116));

// Урок 2. Задача 5
// Найти минимальное число которое больше 300 и нацело делиться на 17
function delenie(startNumber, delitel) {
  var closeCycl = 0;
  for (var i = startNumber; closeCycl < 1; i++) {
    if (i % delitel == 0) {
      closeCycl++;
      return i;
    }
    else if (i<delitel) {
      return console.log("Делимое меньше чем делитель. Невозможно получить результат");
      break;
    }
  }

}
console.log("Ближайшее делимое число на заданное = " + delenie(300, 17));

// Урок 2. Задача 6
// Заданы две переменные для двух целых чисел, найти максимальное общее значение которое нацело делит два заданных числа.
function delitelObshiy(firstNumber, secondNumber) {
  var leastNumber = firstNumber; //Наименьшее число из двух заданных (Для того чтобы начать проверять делители из наименьшего). Нет смысла проверять числа которые больше чем наименьшее из 2 заданных чисел
  if (firstNumber > secondNumber) {
    leastNumber = secondNumber; //Если первое число больше чем второе, то присвоить наименьшее для второго числа
  }
  for (var i = leastNumber; i > 0; i--) { //Цикл который проверяет делится ли первое и второе число без остатка на i начиная с наименьшего числа
    if (firstNumber % i == 0 && secondNumber % i == 0) {
      break;
    }
  }
  return i;
}
console.log("Максимальный общий делитель = " + delitelObshiy(400, 1000));
