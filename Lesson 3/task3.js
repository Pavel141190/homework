"use strict";
// Урок 3. Задача 3
// Написать функцию, которая транспонирует матрицу


// ГЕНЕРИРОВАНИЕ МАССИВА РАЗМЕРОМ gorizontal НА vertikal с максимально возможным заданным числом в массиве
var mas = [];
function ganerateMas(gorizontal, vertikal,maxRandomNumber) {
  for (var i = 0; i < vertikal; i++) {
    mas[i] = [];
    for (var j = 0; j < gorizontal; j++) {
      mas[i][j] = Math.floor(Math.random() * maxRandomNumber + 1);
    }
  }
  console.log("Массив " + gorizontal + "x" + vertikal + ". Наполнен случайными цифрами от 0 до " + maxRandomNumber);
  console.log (mas);
  }


// ПОВОРОТ МАССИВА ИСПОЛЬЗУЯ ФУНКЦИЮ ВЫШЕ ДЛЯ ГЕНЕРАЦИИ МАССИВА
function reverse(gorizontal, vertikal,maxRandomNumber) {
  ganerateMas(gorizontal, vertikal, maxRandomNumber);
  var reverseMas = [];
  for (var i = 0; i < mas.length; i++) {
    for (var j = 0; j < mas[i].length; j++) {
      if (i == 0) {                           //Чтобы создало j вложенных массивов и не обнулило их после того как i станет равно 1
        reverseMas[j] = [];                   //Создаст нам кол-во вложенных массивов равное кол-ву элеметом сгенерированного массива для нашего перевернутого
      }
      reverseMas[j][i] = mas[i][j];           //Записывает в перевернутый массив элементы как будто не по горизонтали, а по вертикали (т.е. 1-2 элемент нашего массива = 2-1 элементу сгенерированного)
    }
  }
  console.log("Перевернутый массив");
  return reverseMas;
}
console.log (reverse(4,3,11));
