"use strict";
// Урок 3. Задача 2
// Все скрипты которые используют в своей основе цикл - написать с помощью рекурсивных функций


// Урок 2. Задача 1
// Переменная содержит в себе строку. Вывести строку в обратном порядке.
function reverse(text, fullName, i) {
  if (fullName === undefined) {
    fullName = "";
    i = text.length - 1;
  }
  if (i > -1) {
    fullName += text[i--];
    return reverse(text, fullName, i);
  }
  else {
    return fullName;
  }
}
console.log("Перевернутый текст = " + reverse("ТОРОВЕРЕП"));

// Урок 2. Задача 2
// Переменная содержит в себе число. Написать скрипт который посчитает факториал этого числа.
function factorial(number, result, i) {
  if (result === undefined) {
    result = 1;
    i = 1;
  }
  if (i <= number) {
    result *= i++;
    return factorial(number, result, i);
  }
  else {
    return (result);
  }
}
console.log("Факториал равен " + factorial(5));

// Урок 2. Задача 3
// Дано число - вывести первые N делителей этого числа нацело.
function firstDeliteli(number, kolDeliteley, i, n, masDeliteley) {
  if (n === undefined) {
    n = 0;
    i = 1;
    var masDeliteley = [];
  }
  if (number % i == 0 && n < kolDeliteley) {
    n++;
    masDeliteley.push(i);
    i++;
    return firstDeliteli(number, kolDeliteley, i, n, masDeliteley);
  }
  else if (number % i != 0 && i < number / 2) {
    i++;
    return firstDeliteli(number, kolDeliteley, i, n, masDeliteley);
  }
  else {
    if (n < kolDeliteley) {
      console.log("Делителей меньше чем задано")
    }
    return masDeliteley;
  }

}
console.log("Первые делители " + firstDeliteli(99, 5));

// Урок 2. Задача 4
// Найти сумму цифр числа которые кратны двум
function summaKratnih(number, i, result, numberString) {
  if (result == undefined) {
    result = 0;
    numberString = number + "";
    i = 0;
  }
  if (numberString[i] % 2 === 0 && numberString.length > i) {
    result += parseInt(numberString[i]);
    i++;
    return (summaKratnih(number, i, result, numberString));
  } else if (numberString[i] % 2 !== 0 && numberString.length > i) {
    i++;
    return (summaKratnih(number, i, result, numberString));
  } else {
    return result;
  }
}
console.log("Сумма цифр числа равняется " + summaKratnih(11222424111));

// Урок 2. Задача 5
// Найти минимальное число которое больше 300 и нацело делиться на 17

function delenieNacelo (startNumber, delitel){
          if (startNumber%delitel === 0) {
            var result = startNumber;
            return (result);
          }
          else if (startNumber%delitel !== 0) {
            startNumber++;
            return delenieNacelo (startNumber, delitel);
          }
}

console.log("Ближайшее число которое делится на заданное и больше заданного = " + delenieNacelo(300,17));

// Урок 2. Задача 6
// Заданы две переменные для двух целых чисел, найти максимальное общее значение которое нацело делит два заданных числа.
function obshiyDelitel (firstNumber,secondNumber,delitel) {
  if (delitel === undefined) {
    if (firstNumber>secondNumber) {
      delitel = secondNumber;
    }
    else {
      delitel = firstNumber;
    }
  }
  if (firstNumber%delitel == 0 && secondNumber%delitel ==0) {
    return delitel;
  }
  else {
    delitel--;
    return obshiyDelitel (firstNumber,secondNumber,delitel);
  }
}
console.log("Общий делитель для заданных чисел = " + obshiyDelitel(1200,800));
