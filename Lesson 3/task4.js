"use strict";
// Урок 3. Задача 4
// Написать функцию, которая складывает две матрицы


// ГЕНЕРИРОВАНИЕ МАССИВА РАЗМЕРОМ gorizontal НА vertikal с максимально возможным заданным числом в массиве

function ganerateMas(gorizontal, vertikal,maxRandomNumber) {
  var mas = [];
  for (var i = 0; i < vertikal; i++) {
    mas[i] = [];
    for (var j = 0; j < gorizontal; j++) {
      mas[i][j] = Math.floor(Math.random() * maxRandomNumber + 1);
    }
  }
  console.log("Массив " + gorizontal + "x" + vertikal + ". Наполнен случайными цифрами от 0 до " + maxRandomNumber);
  console.log (mas);
  return mas;
  }

// ФУНКЦИЯ ДЛЯ СЛОЖЕНИЯ ДВУХ МАТРИЦ ОДИНАКОВОГО РАЗМЕРА
function summaMatrix(gorizontal, vertikal, maxRandomNumber) {
  var summa = [];                                                   //МАССИВ В КОТОРОМ БУДЕТ ХРАНИТЬСЯ СУММА МАТРИЦ
  var matrix1 = ganerateMas(gorizontal, vertikal, maxRandomNumber);
  var matrix2 = ganerateMas(gorizontal, vertikal, maxRandomNumber);
  for (var i = 0; i < vertikal; i++) {
    summa[i] = [];
    for (var j = 0; j < gorizontal; j++) {
      summa[i][j] = matrix1[i][j] + matrix2[i][j];
    }
  }
  console.log ("Сумма матриц");
  return summa;
}
console.log(summaMatrix(3, 3, 10)); //В ПАРАМЕТРЫ ФУНКЦИИ УКАЗЫВАЕМ РАЗМЕР МАССИВА И МАКСИМАЛЬНО ВОЗМОЖНОЕ ЧИСЛО
