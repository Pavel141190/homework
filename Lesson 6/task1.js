// - Посчитать количество ссылок на странице, вывести их содержимое
"use strict"

let zagolovokVivodaJS = document.createElement("H1");
zagolovokVivodaJS.innerHTML = "Динамический вывод JS"
document.querySelector("body").appendChild(zagolovokVivodaJS); //Вывод заголовка который указывает на то что ниже идет динамический код




function podschetSsilok() { //Функция для подсчета ссылок и вывода их содержимого
  let body = document.querySelector("body"); // Переменная которая указывает на body документа
  let kolSsilok = 0; // Переменная для подсчета количества ссылок
  let vseTegi = body.getElementsByTagName("*");
  for (let i = 0; i < vseTegi.length; i++) { //Запускаец цикл, которйы проходится по всем тегам в документе
    if (vseTegi[i].hasAttribute("href") == true) { // Если текущий тег имеет атрибут href - заходим в IF
      let soderjimoeSsilki = document.createElement("H3"); // Переменная которая создает новый h3 тег
      soderjimoeSsilki.innerHTML = body.getElementsByTagName("*")[i].getAttribute("href"); // Запись в созданнйы тег h3 содержимое атрибута href
      body.appendChild(soderjimoeSsilki); // Добавляем внутрь body созданный нами тег h3 с содержимым ссылки
      kolSsilok++; // Увеличиваем количество ссылок на 1
    }
  }
  let resH1 = document.createElement("H2"); // Переменная которая создает тег h2 для того чтобы записать в него количество ссылок на странице
  resH1.innerHTML = "Количество ссылок на странице = " + kolSsilok; // Запись внутрь h2 количества ссылок на странице
  body.appendChild(resH1); // Добавление в HTML созданного h2 с кол-ом ссылок
}

podschetSsilok(); //запуск функции для добавления кол-ва ссылок и их содержимого
