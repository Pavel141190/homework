//Вывести статистику средних оценок в разрезе курса и статистику по количеству неактивных студентов
//в разрезе каждого курса и общее количество неактивных студентов
"use strict"

var masStudents = [{
    name: "Pavel",
    estimate: 4,
    course: 1,
    active: true
  },
  {
    name: "Igor",
    estimate: 4.2,
    course: 2,
    active: true
  },
  {
    name: "Evheniy",
    estimate: 4.4,
    course: 3,
    active: true
  },
  {
    name: "Evheniy",
    estimate: 4.4,
    course: 3,
    active: false
  },
  {
    name: "Inna",
    estimate: 4.1,
    course: 4,
    active: true
  },
  {
    name: "Natalia",
    estimate: 4.6,
    course: 1,
    active: true
  },
  {
    name: "Sergey",
    estimate: 4.2,
    course: 3,
    active: false
  },
  {
    name: "Sergey",
    estimate: 4.2,
    course: 1,
    active: false
  },
  {
    name: "Sergey",
    estimate: 4.2,
    course: 2,
    active: false
  },
  {
    name: "Evheniy",
    estimate: 4.4,
    course: 5,
    active: false
  },
  {
    name: "Sergey",
    estimate: 4.2,
    course: 1,
    active: false
  },
  {
    name: "Sergey",
    estimate: 4.2,
    course: 4,
    active: false
  },
  {
    name: "Alex",
    estimate: 4.8,
    course: 2,
    active: true
  },
  {
    name: "Maxim",
    estimate: 5,
    course: 1,
    active: true
  },
  {
    name: "Alexey",
    estimate: 4.6,
    course: 4,
    active: true
  },
  {
    name: "Anna",
    estimate: 4.9,
    course: 5,
    active: true
  },
  {
    name: "Artem",
    estimate: 4.4,
    course: 4,
    active: true
  },
  {
    name: "Ivan",
    estimate: 4.3,
    course: 5,
    active: true
  },
  {
    name: "Albert",
    estimate: 4.1,
    course: 5,
    active: true
  },
];

let zagolovokVivodaJS = document.createElement("H1");
zagolovokVivodaJS.innerHTML = "Динамический вывод JS"
document.querySelector("body").appendChild(zagolovokVivodaJS); //Вывод заголовка который указывает на то что ниже идет динамический код

function statsStudent() {
  let sredOcenka = 0;
  let kolStudentov = 0;
  let studentsNeativ = 0;
  let allStudentsNeativ = 0;
  for (let j = 1; j<=5; j++) {
    sredOcenka = 0;
    kolStudentov = 0;
    studentsNeativ = 0;
    for (var i = 0; i<masStudents.length; i++) {
      if (masStudents[i].course == j) {
        sredOcenka += masStudents[i].estimate;
        kolStudentov++;
        if (masStudents[i].active == false) {
          studentsNeativ++;
        }

      }

    }
    sredOcenka = Math.floor(sredOcenka/kolStudentov*100)/100;
    var body = document.querySelector("body");
    let text = document.createElement("h4");
    let text2 = document.createElement("h4");
    text.innerHTML = "Средняя оценка " + j + "го курса: " + sredOcenka;
    body.appendChild(text);
    text2.innerHTML = j + " курс: " + studentsNeativ + " неактивных студентов";
    body.appendChild(text2);
allStudentsNeativ +=studentsNeativ;
  }
let text3 = document.createElement("h4");
text3.innerHTML = "Всего неактивных студентов: " + allStudentsNeativ;
body.appendChild(text3);

}
statsStudent(); //Запуск нашей функции
