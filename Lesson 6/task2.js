
// - Посчитать количество тегов “p” на странице которые имеют класс “phrase” - вывести их содержимое
"use strict"

let zagolovokVivodaJS = document.createElement("H1");
zagolovokVivodaJS.innerHTML = "Динамический вывод JS"
document.querySelector("body").appendChild(zagolovokVivodaJS); //Вывод заголовка который указывает на то что ниже идет динамический код



function podschetPhrase() { // Функция для подсчета тегов P с классом phrase и их вывод в HTML
  var body = document.querySelector("body"); // Переменная которая указывает на body документа
  var kolPhrase = 0; // Переменная для подсчета количества тегов
  var kolP = document.querySelectorAll("p"); // Переменная которая выбирает все теги P в документе
  for (let i = 0; i < kolP.length; i++) { // Цикл который проходится по всем тегам P
    for (let j = 0; j < kolP[i].classList.length; j++) { // Цикл который проходит внутри каждого тега P по его классам
      if (document.querySelectorAll("p")[i].classList[j] == "phrase") { // Если внутри тега P есть класс phrase - заходит внутрь
        kolPhrase++; //Увеличиваем кол-во тегов Р с классом phrase
        let soderjimoeP = document.createElement("H3"); // Переменная которая создает новый h3 тег
        soderjimoeP.innerText = kolP[i].innerHTML; // Запись в созданнйы тег h3 содержимое тега Р с классом phrase
        body.appendChild(soderjimoeP); // Добавляем внутрь body созданный нами тег h3 с содержимым HTML
      }
    }
  }
  let resP = document.createElement("h2"); // переменная которая создает тег H2
  resP.innerHTML = ("Количество тэгов P с классом phrase = " + kolPhrase); // Записывает в нашу переменную количество тегов с классом phrase
  body.appendChild(resP); // Добавляет наш тег в HTML
}

podschetPhrase();
