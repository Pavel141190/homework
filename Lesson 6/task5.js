//Добавить текстовое поле ввода - ввод имени студента, поле ввода для курса, оценки и checkbox для
// активности студента, по нажатии на кнопку “Добавить” - студент новый добавляется в список студ
"use strict"

var masStudents = [{
    name: "Pavel",
    estimate: 4,
    course: 1,
    active: true
  },
  {
    name: "Igor",
    estimate: 4.2,
    course: 2,
    active: true
  },
  {
    name: "Evheniy",
    estimate: 4.4,
    course: 3,
    active: true
  },
  {
    name: "Evheniy",
    estimate: 4.4,
    course: 3,
    active: false
  },
  {
    name: "Inna",
    estimate: 4.1,
    course: 4,
    active: true
  }

];

//Вывод заголовка который указывает на то что ниже идет динамический код
let zagolovokVivodaJS = document.createElement("h1");
zagolovokVivodaJS.innerHTML = "Динамический вывод JS"
document.querySelector("body").appendChild(zagolovokVivodaJS);

let checkbox = document.createElement("div"); // Создаем новый блок в который поместим наши формы
checkbox.innerHTML = '<input class = "status" type="checkbox" name="a" value="" checked = "true">'; // Добавляем в наш блок чекбокс
checkbox.innerHTML += '<input class = "name" type="text" placeholder = "Введите имя" name="a" value="">'; // Дальше добавляем поле для ввода имени
checkbox.innerHTML += '<input class = "estimate" type="text" placeholder = "Введите среднюю оценку" name="a" value="">'; // Добавляем поле для оценки
checkbox.innerHTML += '<input class = "course" type="text" placeholder = "Введите курс" name="a" value="">'; // Добавляем поле для ввода курса
checkbox.innerHTML += "<button>Добавить"; //Добавляем кнопку для добавления в таблицу и массив
document.querySelector("body").appendChild(checkbox); // добавляем все созданное выше в HTML код
var body = document.querySelector("body"); // Переменная которая указывает на body
let tablicaHead = document.createElement("table"); // Переменная которая создает таблицу в html
tablicaHead.innerHTML = "<tr class = 'head'><td>Имя<td>Оценка<td>Курс<td>Статус"; //Создаем шапку таблицы
body.appendChild(tablicaHead); //Добавляем шапку в HTML код
document.querySelector("button").addEventListener("click", function(event) { // Функция которая реагирует на нажатие кнопки
  let tablica = document.createElement("tr"); // Переменная которая создает строку для таблицы
  let input = document.querySelectorAll("input"); // Переменная которая указывает на все инпуты в документе
  tablica.innerHTML += "<td>" + input[1].value; // Добавление в таблицу имени которое написано в форме
  tablica.innerHTML += "<td>" + input[2].value; // Добавление в таблицу средней оценки которое написано в форме
  tablica.innerHTML += "<td>" + input[3].value; // Добавление в таблицу курса которое написано в формеов
  if (document.querySelectorAll("input")[0].checked == true) { // Если чекбокс активен значит
    tablica.innerHTML += "<td>Активный"; // Добавляем в таблицу что студент активен
  }
  else { //Иначе
    tablica.innerHTML += "<td>Неактивный"; //Добавляем в таблицу что студент неактивен
  }
  document.querySelector("tbody").appendChild(tablica); //Добавляем строку с заполненными данными в таблицу

    masStudents.push({ //Добавляем в наш массив студентов новый объект с данными указаными в инпутах
      name: input[1].value, //добавление имени в объект нового студента
      estimate: Number(input[2].value), //добавление оценки в объект нового студента
      course: Number(input[3].value), //добавление курса в объект нового студента
      active: document.querySelectorAll("input")[0].checked //добавление имени в объект нового студента
    })
    input[1].value = ""; //Очищение поля для ввода данных
    input[2].value = ""; //Очищение поля для ввода данных
    input[3].value = ""; //Очищение поля для ввода данных

});
