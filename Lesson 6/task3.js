// В задании из пятого урока, взять массив со студентами и вывести их на страницу согласно сверстанной HTML-структуре,
// рядом с каждым студентом вывести крестик - по нажатию на который студент будет удален (удаляется как со страницы, так
// и с объекта), если был удален последний студент написать текстовое сообщение (“Студенты не найдены”)
"use strict"

var masStudents = [{
    name: "Pavel",
    estimate: 4,
    course: 1,
    active: true
  },
  {
    name: "Igor",
    estimate: 4.2,
    course: 2,
    active: true
  },
  {
    name: "Evheniy",
    estimate: 4.4,
    course: 3,
    active: true
  },
  {
    name: "Evheniy",
    estimate: 4.4,
    course: 3,
    active: false
  },
  {
    name: "Inna",
    estimate: 4.1,
    course: 4,
    active: true
  },
  {
    name: "Natalia",
    estimate: 4.6,
    course: 1,
    active: true
  },
  {
    name: "Sergey",
    estimate: 4.2,
    course: 3,
    active: false
  },
  {
    name: "Sergey",
    estimate: 4.2,
    course: 1,
    active: false
  },
  {
    name: "Sergey",
    estimate: 4.2,
    course: 2,
    active: false
  },
  {
    name: "Evheniy",
    estimate: 4.4,
    course: 5,
    active: false
  },
  {
    name: "Sergey",
    estimate: 4.2,
    course: 1,
    active: false
  },
  {
    name: "Sergey",
    estimate: 4.2,
    course: 4,
    active: false
  },
  {
    name: "Alex",
    estimate: 4.8,
    course: 2,
    active: true
  },
  {
    name: "Maxim",
    estimate: 5,
    course: 1,
    active: true
  },
  {
    name: "Alexey",
    estimate: 4.6,
    course: 4,
    active: true
  },
  {
    name: "Anna",
    estimate: 4.9,
    course: 5,
    active: true
  },
  {
    name: "Artem",
    estimate: 4.4,
    course: 4,
    active: true
  },
  {
    name: "Ivan",
    estimate: 4.3,
    course: 5,
    active: true
  },
  {
    name: "Albert",
    estimate: 4.1,
    course: 5,
    active: true
  },
];

function vivodStudents() { // Функция для заполнения и удаления студентов из HTML
  var body = document.querySelector("body"); // Переменная кторая указывает на body HTML
  let tablicaHead = document.createElement("table"); // Переменная которая создает таблицу в html
  let valueNumber = 0; // Переменная для заполнения атрибута value уникальными числами
  tablicaHead.innerHTML = "<tr class = 'head'><td>Галочка<td>Имя<td>Оценка<td>Курс<td>Статус"; // заполняет шапку таблицы
  body.appendChild(tablicaHead); // Добавляет эту шапку в документ html
  for (let i = 0; i < masStudents.length; i++) { // Цикл который заполняет таблицу студентами, а так же в нем происходит прослушивание чекбоксов
    let tablica = document.createElement("tr"); // Переменная которая создает строку в таблице
    tablica.innerHTML += '<td><input type="checkbox" name="a" value=" ' + i + '">' // Заполнение созданной строки данными студентов
    tablica.innerHTML += "<td>" + masStudents[i].name; // Заполнение созданной строки данными студентов
    tablica.innerHTML += "<td>" + masStudents[i].estimate; // Заполнение созданной строки данными студентов
    tablica.innerHTML += "<td>" + masStudents[i].course; // Заполнение созданной строки данными студентов
    tablica.innerHTML += "<td>" + masStudents[i].active; // Заполнение созданной строки данными студентов
    document.querySelector("tbody").appendChild(tablica); //Добавление в html созданной строки с заполненными данными
    document.querySelectorAll("input")[i].addEventListener("click", function(event) { // Прослушивание чекбоксов на клик
      let test = this.value; // переменная котороая получает value чекбокса на который клиенули
      console.log(test);
      test = Number(test) + 1; // Преобразование в число и добавляем 1 так как 1й студент имеет value 0 , а по счету это tr с индексом 1
      masStudents.splice(i, 1); // Удаляем из нашего массива студентов один i элемент
      document.querySelectorAll("tr")[test].innerHTML = ""; // Очищаем строку из таблицы под номером test
      tablicaHead.innerHTML = ""; // Полностью очищаем таблицу чтобы заполнить её заново , но уже без удаленного объекта из массива
      vivodStudents(); //Перезапускаем нашу функцию, которая заполнит таблицу без удаленного объекта
    });
  }
  if (masStudents.length == 0) { //если массив студнетов пустой

    tablicaHead.innerHTML = "<h3>Студены не найдены"; //Пишем в нашу таблицу что студены не найдены
  }
}
vivodStudents(); //Запуск нашей функции
