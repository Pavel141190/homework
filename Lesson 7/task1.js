"use strict"
//При каждом действии удаления или добавления студентов нужно пересчитывать статистику средней
// оценки в разрезе каждого курса и подсчета количества неактивных студентов и изменять соответствующее содержимое.
// В ряде предыдущих заданий - выделять красным цветом тех студентов которые имеют оценку 3 и менее. которые от 3 до 4  - желтым и которые 4 и выше - зеленым.
// Аналогично как в предыдущем задании этого урока отмечать фоновым цветом вывод статистики в разрезе каждого курса касательно средней оценки
// Добавить для каждого студента иконку по нажатию на которую студент переводится в статус неактивный из активного и наоборот - при этом для двух состояний иконки тоже должны быть разными и изменять
// По нажатие на имя студента - удалять имя, вместо имени показывать форму ввода - по нажатию на ENTER сохранять новое имя для этого студента, удалять форму ввода и выводить в списке новое имя студента
// По аналогии предыдущего пункта сделать тоже самое с номером курса и с оценкой студента. Не забыть что при изменении оценки статистика также должна быть пересчитана и выведена новая статистика.

var masStudents = [{
    name: "Pavel",
    estimate: 2,
    course: 1,
    active: true
  },
  {
    name: "Igor",
    estimate: 3,
    course: 2,
    active: true
  },
  {
    name: "Evheniy",
    estimate: 4,
    course: 3,
    active: true
  },
  {
    name: "Evheniy",
    estimate: 5,
    course: 3,
    active: false
  },
  {
    name: "Inna",
    estimate: 2.5,
    course: 4,
    active: true
  },
  {
    name: "Natalia",
    estimate: 3.5,
    course: 1,
    active: true
  },
  {
    name: "Sergey",
    estimate: 4.5,
    course: 3,
    active: false
  },
  {
    name: "Sergey",
    estimate: 2.8,
    course: 1,
    active: false
  },
  {
    name: "Sergey",
    estimate: 3.8,
    course: 2,
    active: false
  },
  {
    name: "Evheniy",
    estimate: 4.8,
    course: 5,
    active: false
  },
  {
    name: "Sergey",
    estimate: 3.1,
    course: 1,
    active: false
  },
  {
    name: "Sergey",
    estimate: 4.1,
    course: 4,
    active: false
  },
  {
    name: "Alex",
    estimate: 5,
    course: 2,
    active: true
  },
  {
    name: "Maxim",
    estimate: 3.5,
    course: 1,
    active: true
  },
  {
    name: "Alexey",
    estimate: 4.6,
    course: 4,
    active: true
  },
  {
    name: "Anna",
    estimate: 4.9,
    course: 5,
    active: true
  },
  {
    name: "Artem",
    estimate: 2.5,
    course: 4,
    active: true
  },
  {
    name: "Ivan",
    estimate: 2.8,
    course: 5,
    active: true
  },
  {
    name: "Albert",
    estimate: 4.1,
    course: 5,
    active: true
  },
];

// Функция для заполнения студентов из массива в html
function outputStudents() {
  var averageScorecourse = 0;
  var averageScoreCourse2 = 0;
  var averageScoreCourse3 = 0; //Переменная которая хранит в себе среднюю оценку курса
  var averageScoreCourse4 = 0;
  var averageScoreCourse5 = 0;
  var numberOfInputs1 = 0;
  var numberOfInputs2 = 0;
  var numberOfInputs3 = 0; //Переменная которая хранит в себе коилчество студентов определенного курса
  var numberOfInputs4 = 0;
  var numberOfInputs5 = 0;
  var numberOfInactive1 = 0;
  var numberOfInactive2 = 0;
  var numberOfInactive3 = 0;
  var numberOfInactive4 = 0;
  var numberOfInactive5 = 0;
  var body = document.querySelector("body"); // Переменная кторая указывает на body HTML

  // Создание блока с полями ввода
  let checkbox = document.createElement("div"); // Создаем новый блок в который поместим наши формы
  checkbox.innerHTML = '<input class = "status" type="checkbox" name="a" value="" checked>'; // Добавляем в наш блок чекбокс
  checkbox.innerHTML += '<input class = "name" type="text" placeholder = "Введите имя" name="a" value="">'; // Дальше добавляем поле для ввода имени
  checkbox.innerHTML += '<input class = "estimate" type="text" placeholder = "Введите среднюю оценку" name="a" value="">'; // Добавляем поле для оценки
  checkbox.innerHTML += '<input class = "course" type="text" placeholder = "Введите курс" name="a" value="">'; // Добавляем поле для ввода курса
  checkbox.innerHTML += "<button>Добавить"; //Добавляем кнопку для добавления в таблицу и массив
  document.querySelector("body").appendChild(checkbox); // добавляем все созданное выше в HTML код

  //Создание таблицы со средними оценками
  let tableStatistic = document.createElement("table"); // Переменная которая создает таблицу в html
  tableStatistic.innerHTML = "<tr class = 'head'><td><td>1 курс<td>2 курс<td>3 курс<td>4 курс<td>5 курс"; // заполняет шапку таблицы
  body.appendChild(tableStatistic); // Добавляет эту шапку в документ html

  // Заполнение HTML стдентами из заданного массива
  let tableHead = document.createElement("table"); // Переменная которая создает таблицу в html
  let valueNumber = 0; // Переменная для заполнения атрибута value уникальными числами
  tableHead.innerHTML = "<tr class = 'head'><td>Удалить<br>студента<td>Имя<td>Оценка<td>Курс<td>Активность"; // заполняет шапку таблицы
  body.appendChild(tableHead); // Добавляет эту шапку в документ html

  // Цикл который заполняет таблицу студентами, а так же в нем происходит прослушивание: чекбоксов на удаление, имя студента, оценок и курса в таблице для создания поля ввода
  for (let i = 0; i < masStudents.length; i++) {

    // Свитч для расчета средней оценки по каждому курсу
    switch (masStudents[i].course) {
      case 1:
        averageScorecourse += masStudents[i].estimate;
        numberOfInputs1++;
        break;
      case 2:
        averageScoreCourse2 += masStudents[i].estimate;
        numberOfInputs2++;
        break;
      case 3:
        averageScoreCourse3 += masStudents[i].estimate;
        numberOfInputs3++;
        break;
      case 4:
        averageScoreCourse4 += masStudents[i].estimate;
        numberOfInputs4++;
        break;
      case 5:
        averageScoreCourse5 += masStudents[i].estimate;
        numberOfInputs5++;
        break;
      default:
    }

    // Свитч для расчета количества неактивных студентов
    if (masStudents[i].active == false) {
      switch (masStudents[i].course) {
        case 1:
          numberOfInactive1++;
          break;
        case 2:
          numberOfInactive2++;
          break;
        case 3:
          numberOfInactive3++;
          break;
        case 4:
          numberOfInactive4++;
          break;
        case 5:
          numberOfInactive5++;
          break;
        default:
      }
    }

    //Заполнение таблицы с данными студентов
    let table = document.createElement("tr"); // Переменная которая создает строку в таблице
    //Добавляем класс для новой строки в зависимости от средней оценки студена
    if (masStudents[i].estimate <= 3) {
      table.classList.add("redLine");
    } else if (masStudents[i].estimate > 3 && masStudents[i].estimate <= 4) {
      table.classList.add("yellowLine");
    } else if (masStudents[i].estimate > 4) {
      table.classList.add("greenLine");
    }

    //Добавляем данные студента в таблицу
    table.innerHTML += '<td><input class = "checkboxCheck" type="checkbox" name="a" value=" ' + i + '">' // Заполнение созданной строки данными студентов
    table.innerHTML += '<td class = "nameCheck">' + masStudents[i].name; // Заполнение созданной строки данными студентов
    table.innerHTML += '<td class = "scoreCheck">' + masStudents[i].estimate; // Заполнение созданной строки данными студентов
    table.innerHTML += '<td class = "courseCheck">' + masStudents[i].course; // Заполнение созданной строки данными студентов
    if (masStudents[i].active == true) { // Добавление дополнительной иконки для измененеия статуса студента
      table.innerHTML += "<td>" + '<img class = "image" src="img/Tick_Mark.svg" alt="альтернативный текст">';
    } else {
      table.innerHTML += "<td>" + '<img class = "image" src="img/73031.svg" alt="альтернативный текст">';
    }
    document.querySelectorAll("tbody")[1].appendChild(table); //Добавление в html созданной строки с заполненными данными

    // Прослушивание чекбоксов с классом checkboxCheck на клик и очищение массива и HTML с выбранными студентами
    document.querySelectorAll(".checkboxCheck")[i].addEventListener("click", function() { // Прослушивание чекбоксов на клик
      let test = this.value; // переменная котороая получает value чекбокса на который клиенули
      test = Number(test) + 1; // Преобразование в число и добавляем 1 так как 1й студент имеет value 0 , а по счету это tr с индексом 1
      masStudents.splice(i, 1); // Удаляем из нашего массива студентов один i элемент
      body.innerHTML = "";
      outputStudents(); //Перезапускаем нашу функцию, которая заполнит таблицу без удаленного объекта
    });

    //Прослушивание при клике на имя студента и создания поля ввода в этой ячейке вместо имени
    document.querySelectorAll(".nameCheck")[i].addEventListener("click", function() {
      if (document.querySelectorAll(".nameCheck")[i].childNodes[0].tagName == undefined) {
        document.querySelectorAll(".nameCheck")[i].innerHTML = '<input class = "name" type="text" placeholder = "Введите имя" name="a" value="">';
      }
    });

    // Прослушивание поля ввода в таблица на нажатие Enter
    document.querySelectorAll(".nameCheck")[i].addEventListener("keydown", function(event) { // Прослушивание чекбоксов на клик
      if (event.key == "Enter") {
        masStudents[i].name = document.querySelectorAll(".nameCheck")[i].childNodes[0].value;
        body.innerHTML = "";
        outputStudents(); //Перезапускаем нашу функцию, которая заполнит таблицу без удаленного объекта
      }
    });

    //Прослушивание при клике на оценку и создания поля ввода в этой ячейке
    document.querySelectorAll(".scoreCheck")[i].addEventListener("click", function() {
      if (document.querySelectorAll(".scoreCheck")[i].childNodes[0].tagName == undefined) {

        document.querySelectorAll(".scoreCheck")[i].innerHTML = '<input class = "name" type="text" placeholder = "Введите оценку" name="a" value="">';
      }
    });

    // Прослушивание поля ввода в таблица на нажатие Enter
    document.querySelectorAll(".scoreCheck")[i].addEventListener("keydown", function(event) { // Прослушивание чекбоксов на клик
      if (event.key == "Enter") {
        masStudents[i].estimate = Number(document.querySelectorAll(".scoreCheck")[i].childNodes[0].value);
        body.innerHTML = "";
        outputStudents(); //Перезапускаем нашу функцию, которая заполнит таблицу без удаленного объекта
      }
    });

    //Прослушивание при клике на оценку и создания поля ввода в этой ячейке
    document.querySelectorAll(".courseCheck")[i].addEventListener("click", function() {
      if (document.querySelectorAll(".courseCheck")[i].childNodes[0].tagName == undefined) {

        document.querySelectorAll(".courseCheck")[i].innerHTML = '<input class = "name" type="text" placeholder = "Введите курс" name="a" value="">';
      }
    });

    // Прослушивание поля ввода в таблица на нажатие Enter
    document.querySelectorAll(".courseCheck")[i].addEventListener("keydown", function(event) { // Прослушивание чекбоксов на клик
      if (event.key == "Enter") {
        masStudents[i].course = Number(document.querySelectorAll(".courseCheck")[i].childNodes[0].value);
        body.innerHTML = "";
        outputStudents(); //Перезапускаем нашу функцию, которая заполнит таблицу без удаленного объекта
      }
    });
  }

  // Расчет средней оценки после окончания цикла (в цикле была получена сумма всех оценок и кол-во студентов для вычисление средней оценки)
  averageScorecourse = Math.floor(averageScorecourse / numberOfInputs1 * 100) / 100;
  averageScoreCourse2 = Math.floor(averageScoreCourse2 / numberOfInputs2 * 100) / 100;
  averageScoreCourse3 = Math.floor(averageScoreCourse3 / numberOfInputs3 * 100) / 100;
  averageScoreCourse4 = Math.floor(averageScoreCourse4 / numberOfInputs4 * 100) / 100;
  averageScoreCourse5 = Math.floor(averageScoreCourse5 / numberOfInputs5 * 100) / 100;

  //Заполнение таблицы со средними оценками
  let courseAverage = document.createElement("tr");
  courseAverage.innerHTML += "<td>" + "Ср. оц.";
  courseAverage.innerHTML += "<td>" + averageScorecourse;
  courseAverage.innerHTML += "<td>" + averageScoreCourse2;
  courseAverage.innerHTML += "<td>" + averageScoreCourse3;
  courseAverage.innerHTML += "<td>" + averageScoreCourse4;
  courseAverage.innerHTML += "<td>" + averageScoreCourse5;
  document.querySelectorAll("tbody")[0].appendChild(courseAverage);

  //Заполнение таблицы количество неактивных студентов
  let inactiveStud = document.createElement("tr");
  inactiveStud.innerHTML += "<td>" + "Неакт. студ.";
  inactiveStud.innerHTML += "<td>" + numberOfInactive1;
  inactiveStud.innerHTML += "<td>" + numberOfInactive2;
  inactiveStud.innerHTML += "<td>" + numberOfInactive3;
  inactiveStud.innerHTML += "<td>" + numberOfInactive4;
  inactiveStud.innerHTML += "<td>" + numberOfInactive5;
  document.querySelectorAll("tbody")[0].appendChild(inactiveStud);

  //Цикл в котором перебираем все ячейки в строке со средники оценками по каждому курсу (таблица средних оценок по каждому курсу). Присваиваем ячейке класс в зависимости от оценки

  for (let i = 0; i < courseAverage.childNodes.length; i++) {
    console.log(courseAverage.childNodes[i].innerHTML);

    if (courseAverage.childNodes[i].innerHTML <= 3) {
      courseAverage.childNodes[i].classList.add("redLine");
    } else if (courseAverage.childNodes[i].innerHTML > 3 && courseAverage.childNodes[i].innerHTML <= 4) {
      courseAverage.childNodes[i].classList.add("yellowLine");
    } else if (courseAverage.childNodes[i].innerHTML > 4) {
      courseAverage.childNodes[i].classList.add("greenLine");
    }
  }

  // Если массив со студентами стал пустым пишем что студенты не найдены добавляя заголовок h2
  if (masStudents.length == 0) { //если массив студнетов пустой
    let finalText = document.createElement("h2");
    finalText.id = "finalText";
    finalText.innerHTML = "В массиве не осталось студентов";
    document.querySelector("body").appendChild(finalText);
  }

  // Прослушивает кнопку добавить на клик и добавляем новых студентов при нажатии как в массив так и в HTML
  document.querySelector("button").addEventListener("click", function() { // Функция которая реагирует на нажатие кнопки
    let table = document.createElement("tr"); // Переменная которая создает строку для таблицы
    let input = document.querySelectorAll("input"); // Переменная которая указывает на все инпуты в документе
    // Если поля для ввода данных заполнены и в полях с оценко и курсом цифры меньше или равно 5
    if (input[1].value != "" && input[2].value != "" && input[3].value != "" && isNaN(input[2].value) == false && isNaN(input[3].value) == false && input[3].value <= 5 && input[2].value <= 5) {
      // Так как после добавления нового стдента в массив у нас идет перезапуск функции в которой есть добавление в HTML студентов из массива. То нам нет
      // смысла добавлять их сразу в HTML код так как он сотрется и будет перезапись

      //Добавление в массив нового студента
      masStudents.push({ //Добавляем в наш массив студентов новый объект с данными указаными в инпутах
        name: input[1].value, //добавление имени в объект нового студента
        estimate: Number(input[2].value), //добавление оценки в объект нового студента
        course: Number(input[3].value), //добавление курса в объект нового студента
        active: document.querySelector(".status").checked //добавление активности в объект нового студента
      });

      //После каждого добавление студентов нужно очистить HTML и внести заново чтобы корректно работал цикл удаления
      body.innerHTML = "";
      outputStudents(); //Перезапускаем нашу функцию, которая заполнит таблицу без удаленного объекта
    } else {
      alert("Заполните правильно все поля!");
    }
  });

  //Цикл в котором мы перебираем картинки и проверям их на клик. Клик меняет активность студента пишет в массив и перезапускает нашу функцию по выводу студентов
  for (let i = 0; i < document.querySelectorAll(".image").length; i++) {
    document.querySelectorAll(".image")[i].addEventListener("click", function() {
      if (masStudents[i].active == true) {
        masStudents[i].active = false;
      } else {
        masStudents[i].active = true;
      }
      body.innerHTML = "";
      outputStudents(); //Перезапускаем нашу функцию, которая заполнит таблицу без удаленного объекта
    })

  }
}
outputStudents(); //Запуск нашей функции
