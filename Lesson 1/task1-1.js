var a = 9;
if (a === 0) {
  alert("Ноль");
} else if (a === 1) {
  alert("Один");
} else if (a === 2) {
  alert("Два");
} else if (a === 3) {
  alert("Три");
} else if (a === 4) {
  alert("Четыре");
} else if (a === 5) {
  alert("Пять");
} else if (a === 6) {
  alert("Шесть");
} else if (a === 7) {
  alert("Семь");
} else if (a === 8) {
  alert("Восемь");
} else if (a === 9) {
  alert("Девять");
} else {
  alert("Число не попадает в интервал от 0 до 9");
}
