"use strict"

function Student() {
  this.student = [];
  this.getRequest();
}

var body = document.querySelector("body"); // Переменная кторая указывает на body HTML


Student.prototype.outputStudents = function(data) {
  console.log("Заполняем HTML студентами");
  let self = this;

  var masStudents = data;
  let studPerHour = 0; //Переменная для расчета сдуентов добавленных за последний час
  //Создание формы для добавления студента
  let checkbox = document.createElement("form"); // Создаем новый блок в который поместим наши формы
  checkbox.innerHTML = '<input class = "status" type="checkbox" name="a" value=""  checked>'; // Добавляем в наш блок чекбокс
  checkbox.innerHTML += '<input class = "name" type="text" placeholder = "Введите имя" name="first_name" value="">'; // Дальше добавляем поле для ввода имени
  checkbox.innerHTML += '<input class = "estimate" type="text" placeholder = "Введите среднюю оценку" name="estimate" value="">'; // Добавляем поле для оценки
  checkbox.innerHTML += '<input class = "course" type="text" placeholder = "Введите курс" name="course" value="">'; // Добавляем поле для ввода курса
  // checkbox.innerHTML += '<input class = "email" type="text" placeholder = "Введите email" name="a" value="">'; // Добавляем поле для ввода курса
  checkbox.innerHTML += '<button type = "button">Добавить'; //Добавляем кнопку для добавления в таблицу и массив
  body.appendChild(checkbox); // добавляем все созданное выше в HTML код

  //Создание таблицы со средними оценками
  let tableStatistic = document.createElement("table"); // Переменная которая создает таблицу в html
  // Создание 1ой строки с курсами
  let stringTab = document.createElement("tr");
  stringTab.classList.add("head");
  stringTab.innerHTML = "<td>Course";
  //Создание 2ой строки со средними оценками
  let stringTab2 = document.createElement("tr");
  stringTab2.classList.add("grades");
  stringTab2.innerHTML = "<td>Grades";
  //Создание 3ой строки с неактивными студентами
  let stringTab3 = document.createElement("tr");
  stringTab3.classList.add("inactive");
  stringTab3.innerHTML = "<td>Inactive";
  //Добавление всех строк в нашу таблицу
  tableStatistic.append(stringTab, stringTab2, stringTab3);
  body.appendChild(tableStatistic);
  //Получение массива с уникальными курсами
    if (masStudents[0] != undefined) {
      var uniqueCourseMas = self.uniqueCourse(masStudents);
    }
    else {
      var uniqueCourseMas = [];
    }
  //Цикл который делает запусков столько сколько уникальных курсов, чтобы для каждого посчитать оценку и неакт студентов
  for (let i in uniqueCourseMas) {
    let averageStudentGrade = 0;
    let numberOfStudents = 0;
    let inactiveStud = 0;
    //Создаем ячейки в шапке таблицы с уникальными курсами
    let cellTable = document.createElement("td");
    cellTable.innerHTML = uniqueCourseMas[i];
    document.getElementsByClassName("head")[0].append(cellTable);
    //Перебираем всех студентов и смотрим их оценки и активность и получаем результат для таблицы
    for (let j in masStudents) {
      //Если уникальный курс совпадает с курсом студента которого мы смотрим во внутреннем цикле - складываем их оценки и увеличиваем кол-во студентов
      if (masStudents[j].course == uniqueCourseMas[i]) {
        averageStudentGrade += masStudents[j].estimate;
        numberOfStudents++;
      }
      //Если студент внутреннего цикла неактивен и его курс равен проверяемому курсу - увеличиваем кол-во неакт студентов на 1
      if (masStudents[j].is_active == false && masStudents[j].course == uniqueCourseMas[i]) {
        inactiveStud++;
      }
    }
    //После того как мы получили сумму всех оценок студентов по текущему курсу и кол-во студентов верхнего цикла - считаем среднюю оценку
    averageStudentGrade = Math.floor(averageStudentGrade / numberOfStudents * 100) / 100;
    //Создаем ячейку и добавляем в неё среднюю оценку
    let cellGrade = document.createElement("td");
    cellGrade.innerHTML = averageStudentGrade;
    //Создаем ячейку и добавляем в неё кол-во неактивных студентов
    let cellInactive = document.createElement("td");
    cellInactive.innerHTML = inactiveStud;
    //Добавляем ячейку с оценкой и неакт студентами в соответственные строки таблицы
    document.getElementsByClassName("grades")[0].append(cellGrade);
    document.getElementsByClassName("inactive")[0].append(cellInactive);
    //Присваиваем класс ячейке с оценкой в зависимости от оценки
    if (averageStudentGrade <= 3) {
      cellGrade.classList.add("redLine");
    } else if (averageStudentGrade > 3 && averageStudentGrade <= 4) {
      cellGrade.classList.add("yellowLine");
    } else if (averageStudentGrade > 4) {
      cellGrade.classList.add("greenLine");
    }

  }
  //Создание таблицы для наполнения данными всех студентов
  let tableHead = document.createElement("table"); // Переменная которая создает таблицу в html
  tableHead.innerHTML = "<tr class = 'head'><td>Удалить<br>студента<td>Имя<td>Оценка<td>Курс<td>Активность"; // заполняет шапку таблицы
  body.appendChild(tableHead);

  // Цикл который заполняет таблицу студентами, а так же в нем происходит прослушивание: чекбоксов на удаление, имя студента, оценок и курса в таблице для создания поля ввода
  for (let i = 0; i < masStudents.length; i++) {
    // // let time = JSON.parse(masStudents[i].time);
    // //Расчет студентов добавленных за последний час. Если у студена заполнено время добавления то заходим в if
    // // let timeNow = Date.now() / 1000 / 60; //Переменная которая хранит в себе текущее время в минутах (кол-во минут с 1970 года)
    // // let timeStudent = time.allTime / 1000 / 60; //Переменная которая хранит в себе время добавления текущего студента в минутах (кол-во минут с 1970 года)
    // if (timeNow - timeStudent < 60) { //если разница меньше 60 минут то увеличиваем переменную кол-ва добавленных за час на 1
    //   studPerHour++;
    // }

    //Заполнение таблицы с данными студентов
    let table = document.createElement("tr"); // Переменная которая создает строку в таблице
    //Добавляем класс для новой строки в зависимости от средней оценки студена
    if (masStudents[i].estimate <= 3) {
      table.classList.add("redLine");
    } else if (masStudents[i].estimate > 3 && masStudents[i].estimate <= 4) {
      table.classList.add("yellowLine");
    } else if (masStudents[i].estimate > 4) {
      table.classList.add("greenLine");
    }

    //Добавляем данные студента в таблицу

    table.innerHTML += '<td><input class = "checkboxCheck" type="checkbox" name="a" value=" ' + i + '">' // Заполнение созданной строки данными студентов
    table.innerHTML += '<td class = "nameCheck">' + masStudents[i].first_name; // Заполнение созданной строки данными студентов
    table.innerHTML += '<td class = "scoreCheck">' + masStudents[i].estimate; // Заполнение созданной строки данными студентов
    table.innerHTML += '<td class = "courseCheck">' + masStudents[i].course; // Заполнение созданной строки данными студентов
    // table.innerHTML += '<td class = "emailCheck">' + masStudents[i].email; // Заполнение созданной строки данными студентов
    if (masStudents[i].is_active == true) { // Добавление дополнительной иконки для измененеия статуса студента
      table.innerHTML += "<td>" + '<img class = "image" src="img/Tick_Mark.svg" alt="альтернативный текст">';
    } else {
      table.innerHTML += "<td>" + '<img class = "image" src="img/73031.svg" alt="альтернативный текст">';
    }
    //Если значение минут меньше 10 - ставим 0 перед числом
    // if (time.minutes < 10) {
    //   time.minutes = "0" + time.minutes;
    // }
    // if (time.hours < 10) {
    //   time.hours = "0" + time.hours;
    // }
    // let month = time.month + 1; //Переменная для приведения месяца в стандарт. JS выдаем на месяца от 0 до 11
    // if (month < 10) {
    //   month = "0" + month;
    // }
    // table.innerHTML += "<td>" + time.hours + ":" + time.minutes + " " + time.day + "." + month + "." + time.year;
    document.querySelectorAll("tbody")[0].appendChild(table); //Добавление в html созданной строки с заполненными данными

    // Прослушивание чекбоксов с классом checkboxCheck на клик и удаление студена из массива с выбранными студентами
    document.querySelectorAll(".checkboxCheck")[i].addEventListener("click", function() { // Прослушивание чекбоксов на клик
      self.studDel(masStudents, i);
    });

    //Прослушивание при клике на имя студента и создания поля ввода в этой ячейке вместо имени.
    let nameCheck = document.querySelectorAll(".nameCheck");
    nameCheck[i].addEventListener("click", function() {
      if (nameCheck[i].childNodes[0].tagName == undefined) {
        nameCheck[i].innerHTML = '<input class = "name" type="text"  name="a" value="" style="width:90px;">';
        nameCheck[i].querySelector("input").value = masStudents[i].first_name;
        nameCheck[i].firstChild.focus();
      }
      nameCheck[i].firstChild.addEventListener("blur", function(event) { // Прослушивание чекбоксов на клик
        nameCheck[i].innerHTML = masStudents[i].first_name;
      });
    });

    // Прослушивание поля ввода в таблица на нажатие Enter
    nameCheck[i].addEventListener("keydown", function(event) { // Прослушивание чекбоксов на клик
      let testName = /^[A-ZА-Я][a-zа-я]{1,14}$/.test(nameCheck[i].childNodes[0].value);
      if (event.key == "Enter" && testName) {
        masStudents[i].first_name = nameCheck[i].childNodes[0].value;
        localStorage.setItem("masStudents", JSON.stringify(masStudents));
        body.innerHTML = "";
        self.editStud(masStudents[i],i);
      } else if (event.key == "Enter") {
        alert("Введите имя с большой буквы используя только буквы")
      }
    });

    //Прослушивание при клике на оценку и создания поля ввода в этой ячейке
    var scoreCheck = document.querySelectorAll(".scoreCheck");
    scoreCheck[i].addEventListener("click", function() {
      if (scoreCheck[i].childNodes[0].tagName == undefined) {
        scoreCheck[i].innerHTML = '<input class = "name" type="text" name="a" value="" style="width:30px;">';
        scoreCheck[i].querySelector("input").value = masStudents[i].estimate;
        scoreCheck[i].firstChild.focus();
      }
      scoreCheck[i].firstChild.addEventListener("blur", function(event) { // Прослушивание чекбоксов на клик
        scoreCheck[i].innerHTML = masStudents[i].estimate;
      });
    });

    // Прослушивание поля ввода оценки в таблице на нажатие Enter
    scoreCheck[i].addEventListener("keydown", function(event) { // Прослушивание чекбоксов на клик
      // let testEstimate = /^[1-5]$/.test(scoreCheck[i].childNodes[0].value); (не работает если вводить дробные числа)
      let testEstimate1 = scoreCheck[i].childNodes[0].value <= 5;
        let testEstimate2 = scoreCheck[i].childNodes[0].value > 0;
      if (event.key == "Enter" && testEstimate1 && testEstimate2) {
        masStudents[i].estimate = Number(scoreCheck[i].childNodes[0].value);
        localStorage.setItem("masStudents", JSON.stringify(masStudents));
        body.innerHTML = "";
        self.editStud(masStudents[i],i);
      } else if (event.key == "Enter") {
        alert("Введите оценку от 1 до 5")
      }
    });

    //Прослушивание при клике на почту email и создания поля ввода в этой ячейке
    // let emailCheck = document.querySelectorAll(".emailCheck");
    // // let testMail = /^.{1,20}\@{1}[a-z]{1,9}(\.[a-z]{2,4}){1,2}$/.test(emailCheck[i].childNodes[0].value);
    // emailCheck[i].addEventListener("click", function() {
    //   if (emailCheck[i].childNodes[0].tagName == undefined) {
    //     emailCheck[i].innerHTML = '<input class = "mailTest" type="text"  name="a" style="width:160px;">';
    //     emailCheck[i].querySelector("input").value = masStudents[i].email;
    //     emailCheck[i].firstChild.focus();
    //   }
    //   emailCheck[i].firstChild.addEventListener("blur", function(event) { // Прослушивание чекбоксов на клик
    //     emailCheck[i].innerHTML = masStudents[i].email;
    //   });
    // });
    //
    // // Прослушивание поля ввода почты в таблице на нажатие Enter
    // emailCheck[i].addEventListener("keydown", function(event) { // Прослушивание чекбоксов на клик
    //   let testMail = /^.{1,20}\@{1}[a-z]{1,9}(\.[a-z]{2,4}){1,2}$/.test(emailCheck[i].childNodes[0].value);
    //   if (event.key == "Enter" && testMail) {
    //     masStudents[i].email = emailCheck[i].childNodes[0].value;
    //     localStorage.setItem("masStudents", JSON.stringify(masStudents));
    //     body.innerHTML = "";
    //     pavel.getRequest(); //Перезапускаем нашу функцию, которая заполнит таблицу без удаленного объекта
    //   } else if (event.key == "Enter") {
    //     alert("Введите корректно email")
    //   }
    // });

    //Прослушивание при клике на оценку и создания поля ввода в этой ячейке
    let courseCheck = document.querySelectorAll(".courseCheck");
    courseCheck[i].addEventListener("click", function() {
      if (courseCheck[i].childNodes[0].tagName == undefined) {
        courseCheck[i].innerHTML = '<input class = "name" type="text"  name="a" style="width:30px;">';
        courseCheck[i].querySelector("input").value = masStudents[i].course;
        courseCheck[i].firstChild.focus();
      }
      courseCheck[i].firstChild.addEventListener("blur", function(event) { // Прослушивание чекбоксов на клик
        courseCheck[i].innerHTML = masStudents[i].course;
      });
    });

    // Прослушивание поля ввода в таблица на нажатие Enter
    courseCheck[i].addEventListener("keydown", function(event) { // Прослушивание чекбоксов на клик
      let testCourse = /^[1-5]$/.test(courseCheck[i].childNodes[0].value);
      if (event.key == "Enter" && testCourse) {
        masStudents[i].course = Number(courseCheck[i].childNodes[0].value);
        localStorage.setItem("masStudents", JSON.stringify(masStudents));
        body.innerHTML = "";
        self.editStud(masStudents[i],i);
      } else if (event.key == "Enter") {
        alert("Введите курс от 1 до 5");
      }
    });
    //Прослушиваем все картинки на клик и меняем в массиве активность и обновляем все в html
    document.querySelectorAll(".image")[i].addEventListener("click", function() {
      if (masStudents[i].is_active == true) {
        masStudents[i].is_active = false;
      } else {
        masStudents[i].is_active = true;
      }
      localStorage.setItem("masStudents", JSON.stringify(masStudents));
      body.innerHTML = "";
      self.editStud(masStudents[i],i);
    });
  }

  //Добавление надписи с количество студентов за последний час
  // let hourSrudents = document.createElement("h2");
  // hourSrudents.innerHTML = "За последний час добавлено: " + studPerHour + " студенов";
  // body.appendChild(hourSrudents);

  //Прослушивание кнопки Добавить на клик и добавление студента в localstorage
  document.querySelector("button").addEventListener("click", function() { // Функция которая реагирует на нажатие кнопки
    let table = document.createElement("tr"); // Переменная которая создает строку для таблицы
    let input = document.querySelectorAll("input"); // Переменная которая указывает на все инпуты в документе
    let testEstimate1 = input[2].value <= 5;
    let testEstimate2 = input[2].value > 0;
    // let testMail = /^.{1,20}\@{1}[a-z]{1,9}(\.[a-z]{2,4}){1,2}$/.test(input[4].value); //В начале от 1 до 20 любых символов, потом 1 собака, от 1 до 9 букв от а до z, потом точка и от а до z 2-4 символа. может быть 1 или 2 раза
    let testName = /^[A-ZА-Я][a-zа-я]{1,14}$/.test(input[1].value); // Первая буква большая и еще + 14 маленьких букв (можно было сделать преобразование любого текста в 1 большую и остальные маленькие буквы)
    // let testEstimate = /^[1-5]$/.test(input[2].value); Не подходит так как не принимает дробные значения
    let testCourse = /^[1-5]$/.test(input[3].value);
    if ( testName && testCourse && testEstimate1 && testEstimate2) {
    //Добавление в localStorage нового студента
    let student = { //Добавляем в наш массив студентов новый объект с данными указаными в инпутах
      first_name: input[1].value, //добавление имени в объект нового студента
      estimate: Number(input[2].value), //добавление оценки в объект нового студента
      course: Number(input[3].value), //добавление курса в объект нового студента
      is_active: document.querySelector(".status").checked //добавление активности в объект нового студента
      // time: JSON.stringify({
      //   allTime: Date.now(),
      //   hours: (new Date).getHours(),
      //   minutes: (new Date).getMinutes(),
      //   year: (new Date).getFullYear(),
      //   month: (new Date).getMonth(),
      //   day: (new Date).getDate()
      // })
    };
    self.addStudent(student);
    } else {
      alert("Заполните корректно все поля");
    }
  });
}

//Функция на добавление студента на сервер
Student.prototype.addStudent = function(student) {
  console.log("Запрос на добавление нового студента");
  let myRequest2 = new XMLHttpRequest();
  myRequest2.open('POST', 'https://evgeniychvertkov.com/api/student/', true);
  myRequest2.setRequestHeader("Content-type", "application/json; charset=utf-8");
  myRequest2.setRequestHeader("X-Authorization-Token", "bde23f58-341b-11eb-a483-f1c3feb07438");
  myRequest2.onreadystatechange = function() {
    if (myRequest2.readyState != 4) {
      return;
    }
    if (myRequest2.status == 200) {
      let requestObj = JSON.parse(myRequest2.responseText);
      localStorage.setItem("masStudents", JSON.stringify(requestObj.students));
      body.innerHTML = "";
      pavel.getRequest();
    } else {
      //Если в localStorage нет ключа masStudents - создаем его
      if (localStorage.getItem("masStudents") == null) {
        localStorage.setItem("masStudents", "[]");
      }
      var masStudents = JSON.parse(localStorage.getItem("masStudents"));
      masStudents.push(student);
      body.innerHTML = "";
      localStorage.setItem("masStudents", JSON.stringify(masStudents));
      pavel.outputStudents(masStudents);

    }

  }
  myRequest2.send(JSON.stringify(student));
}

//Функция для получения массива с уникальными курсами
Student.prototype.uniqueCourse = function(masStudents) {

    let uniqueCourse = [masStudents[0].course];

  for (let i = 1; i < masStudents.length; i++) {
    let flag = true;
    for (let j = 0; j < uniqueCourse.length; j++) {
      if (masStudents[i].course == uniqueCourse[j]) {
        flag = false;
      }
    }
    if (flag == true) {
      uniqueCourse.push(masStudents[i].course);
    }
  }
  uniqueCourse.sort(function(a, b) { //Сортировка массива по возрастанию. Какая-то функция из интернета
    return a - b;
  });
  return uniqueCourse;

}

//Функция удаления студента с сервера
Student.prototype.studDel = function(masStudents, i) {
  console.log("Запрос на удаление данных с сервера");
  let myRequestDel = new XMLHttpRequest();
  let self = this;
  myRequestDel.open('DELETE', 'https://evgeniychvertkov.com/api/student/' + masStudents[i].id + "/", true);
  myRequestDel.setRequestHeader("Content-type", "application/json; charset=utf-8");
  myRequestDel.setRequestHeader("X-Authorization-Token", "bde23f58-341b-11eb-a483-f1c3feb07438");
  myRequestDel.send();
  myRequestDel.onreadystatechange = function() {
    if (myRequestDel.readyState != 4) {
      return;
    }
    if (myRequestDel.status == 200) {
      let requestObj = JSON.parse(myRequestDel.responseText);
      localStorage.setItem("masStudents", JSON.stringify(masStudents));
      body.innerHTML = "";
      pavel.getRequest();
    } else {
      //Если в localStorage нет ключа masStudents - создаем его
      if (localStorage.getItem("masStudents") == null) {
        localStorage.setItem("masStudents", "[]");
      }
      console.log("При удалении что-то пошло не так");
      masStudents = JSON.parse(localStorage.getItem("masStudents"));
      masStudents.splice(i,1);
      body.innerHTML = "";
      localStorage.setItem("masStudents", JSON.stringify(masStudents));
      self.outputStudents(masStudents);
    }
  }
}

//Функция поулчения данных с сервера
Student.prototype.getRequest = function() {
  console.log("Запрос на получение данных с сервера");
  let self = this;
  let myRequest = new XMLHttpRequest();
  myRequest.open('GET', 'https://evgeniychvertkov.com/api/student/', true);
  myRequest.setRequestHeader("Content-type", "application/json; charset=utf-8");
  myRequest.setRequestHeader("X-Authorization-Token", "bde23f58-341b-11eb-a483-f1c3feb07438");
  myRequest.send();
  myRequest.onreadystatechange = function() {
    if (myRequest.readyState != 4) {
      return;
    }
    if (myRequest.status == 200) {
      let requestObj = JSON.parse(myRequest.responseText);
      localStorage.setItem("masStudents", JSON.stringify(requestObj.students));
      self.outputStudents(requestObj.students);
    } else {
      var masStudents = JSON.parse(localStorage.getItem("masStudents")); // Переменная которая указывает на массив студентов в localstorage , но уже преобразован из строки в ма
      //Если в localStorage нет ключа masStudents - создаем его
      if (localStorage.getItem("masStudents") == null || localStorage.getItem("masStudents") == undefined) {
        localStorage.setItem("masStudents", "[]");
      }
      console.log("Сервер недоступен. Используем localStorage. Данные на сервер не перенесутся");
        self.outputStudents(masStudents);
    }
  }
}

//Функция для замены данных студента
Student.prototype.editStud = function(student,i) {
  console.log("Запрос на редактирование данных на сервере");
  let self = this;
  let myRequest = new XMLHttpRequest();
  myRequest.open('PUT', 'https://evgeniychvertkov.com/api/student/', true);
  myRequest.setRequestHeader("Content-type", "application/json; charset=utf-8");
  myRequest.setRequestHeader("X-Authorization-Token", "bde23f58-341b-11eb-a483-f1c3feb07438");
  myRequest.onreadystatechange = function() {
    if (myRequest.readyState != 4) {
      return;
    }
    if (myRequest.status == 200) {
      let requestObj = JSON.parse(myRequest.responseText);
      localStorage.setItem("masStudents", JSON.stringify(requestObj.students));
      pavel.getRequest();
    } else {
      var masStudents = JSON.parse(localStorage.getItem("masStudents")); // Переменная которая указывает на массив студентов в localstorage , но уже преобразован из строки в ма
      //Если в localStorage нет ключа masStudents - создаем его
      if (localStorage.getItem("masStudents") == null) {
        localStorage.setItem("masStudents", "[]");
      }
      var masStudents = JSON.parse(localStorage.getItem("masStudents"));
      masStudents[i] = student;
      body.innerHTML = "";
      localStorage.setItem("masStudents", JSON.stringify(masStudents));
      pavel.outputStudents(masStudents);
    }
  }
  myRequest.send(JSON.stringify(student));
}



let pavel = new Student();
