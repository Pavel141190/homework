function Student (){
    this.students = [];
}

Student.prototype.render = function(){
    let tbody = document.querySelector(".students tbody");
    tbody.innerHTML = "";

    let self = this;

   for(let i = 0; i < this.students.length; i++){
     let tr = document.createElement("TR");

     if(this.students[i].is_active){
         tr.className = "active";
     }else{
         tr.className = "not-active";
     }

     let td = document.createElement("TD");
     td.innerHTML = this.students[i].first_name;
     tr.appendChild(td);

     td = document.createElement("TD");
     td.innerHTML = this.students[i].course;
     tr.appendChild(td);

     td = document.createElement("TD");
     td.innerHTML = this.students[i].estimate;
     tr.appendChild(td);

     td = document.createElement("TD");
     td.innerHTML = "Удалить";
     td.addEventListener("click", (function(index) {
         return function(event){
            self.removeOnServer(index);
            self.render();
        };
    })(i));
     tr.appendChild(td);

     tbody.appendChild(tr);
   }
};

Student.prototype.remove = function(index){
    this.students.splice(index, 1);
};

Student.prototype.removeOnServer = function(index){
    let self = this;
    function callback(response){
        if(!response.is_error){
            self.remove(index);
            self.render();
        }
    }

    this.send("DELETE", 
    'https://evgeniychvertkov.com/api/student/' + this.students[index].id + '/', 
    callback);
};

Student.prototype.send = function(method, url, callback, obj){
    let self = this;

    let data = obj || false;

    var xhr = new XMLHttpRequest();

    xhr.open(method, url, true);

    xhr.setRequestHeader("Content-type", "application/json; charset=utf-8");
    xhr.setRequestHeader("X-Authorization-Token", "bde23f58-341b-11eb-a483-f1c3feb07438");

    if(data){
        xhr.send(JSON.stringify(data));
    }else{
        xhr.send();
    }
    
    xhr.onreadystatechange = function() {
        if (xhr.readyState != 4) return;
        if (xhr.status == 200) {
            let response = JSON.parse(xhr.responseText);
            callback(response);
        }
    }
};

Student.prototype.getAll = function(){
    let self = this;
    function callback(response){
        if(!response.is_error){
            self.students = response.students;
            self.render();
        }
    }
    this.send("GET", 
   'https://evgeniychvertkov.com/api/student/', 
    callback);
};

Student.prototype.add = function(student){
    let self = this;
    function callback(response){
        if(!response.is_error){
            self.students.push(response.student);
            self.render();
        }
    }
    this.send("POST", 
   'https://evgeniychvertkov.com/api/student/', 
    callback, student);
};

window.onload = function(){
    let student = new Student();
    student.getAll();

    let button = document.getElementById("add");

    button.addEventListener("click", function(event){
        let data = new FormData(event.target.closest("form"));
        student.add({
            first_name: data.get("first_name"),
            course: data.get("course"),
            estimate: data.get("estimate"),
            is_active: data.get("is_active") !== null
        });
    });
};
