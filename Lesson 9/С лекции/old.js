
window.onload = function(){
    let students = [
        {
            firstname: "Ivan",
            course: 2,
            avg_ball: 4.4,
        },
        {
            firstname: "Alex",
            course: 2,
            avg_ball: 3.4,
        },
        {
            firstname: "Nikita",
            course: 3,
            avg_ball: 4.3,
        },{
            firstname: "Nikita2",
            course: 3,
            avg_ball: 4.3,
        },{
            firstname: "Nikita3",
            course: 3,
            avg_ball: 4.3,
        },{
            firstname: "Nikita4",
            course: 3,
            avg_ball: 4.3,
        },{
            firstname: "Nikita5",
            course: 3,
            avg_ball: 4.3,
        },
    ];

    function renderStudents(){
        var ul = document.createElement("UL");
        for(let i = 0; i < students.length; i++){
            var li = document.createElement("LI");
            li.innerHTML = students[i].firstname;

            (function (index){
                li.addEventListener("click", function(event){
                    students.splice(index, 1);
                    renderStudents();
                });
            })(i);

            ul.appendChild(li);
        }

        let list = document.querySelector(".list");
        list.innerHTML = "";
        list.appendChild(ul);
    }

    document.querySelector(".create").addEventListener("click", function(event){
        renderStudents();
    });
};