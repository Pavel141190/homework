"use strict"
// Все данные хранить в localStorage касательно студентов
// При открытии страницы выбирать из localstorage и показывать список студентов и статистику соответственно
// Если localstorage не имеет никаких данных в себе, показывать что ничего нет при добавлении из формы нового студента - данные должны попасть и
// сохраниться в localstorage, при следующем открытии страницы - уже подтянуться сохраненные данные

//Если в localStorage нет ключа masStudents - создаем его
if (localStorage.getItem("masStudents") == null) {
  localStorage.setItem("masStudents", "[]");
  console.log(localStorage.getItem("masStudents"));
}

// Функция для заполнения студентов из массива в html
function outputStudents() {

  var masStudents = JSON.parse(localStorage.getItem("masStudents")); // Переменная которая указывает на массив студентов в localstorage , но уже преобразован из строки в массив
  var studPerHour = 0;
  var averageScorecourse = 0;
  var averageScoreCourse2 = 0;
  var averageScoreCourse3 = 0; //Переменная которая хранит в себе среднюю оценку курса
  var averageScoreCourse4 = 0;
  var averageScoreCourse5 = 0;
  var numberOfInputs1 = 0;
  var numberOfInputs2 = 0;
  var numberOfInputs3 = 0; //Переменная которая хранит в себе коилчество студентов определенного курса
  var numberOfInputs4 = 0;
  var numberOfInputs5 = 0;
  var numberOfInactive1 = 0;
  var numberOfInactive2 = 0;
  var numberOfInactive3 = 0;
  var numberOfInactive4 = 0;
  var numberOfInactive5 = 0;
  var body = document.querySelector("body"); // Переменная кторая указывает на body HTML

  // Создание блока с полями ввода
  let checkbox = document.createElement("form"); // Создаем новый блок в который поместим наши формы
  checkbox.innerHTML = '<input class = "status" type="checkbox" name="a" value=""  checked>'; // Добавляем в наш блок чекбокс
  checkbox.innerHTML += '<input class = "name" type="text" placeholder = "Введите имя" name="a" value="">'; // Дальше добавляем поле для ввода имени
  checkbox.innerHTML += '<input class = "estimate" type="text" placeholder = "Введите среднюю оценку" name="a" value="">'; // Добавляем поле для оценки
  checkbox.innerHTML += '<input class = "course" type="text" placeholder = "Введите курс" name="a" value="">'; // Добавляем поле для ввода курса
  checkbox.innerHTML += '<input class = "email" type="text" placeholder = "Введите email" name="a" value="">'; // Добавляем поле для ввода курса
  checkbox.innerHTML += '<button type = "button">Добавить'; //Добавляем кнопку для добавления в таблицу и массив
  body.appendChild(checkbox); // добавляем все созданное выше в HTML код

  //Создание таблицы со средними оценками
  let tableStatistic = document.createElement("table"); // Переменная которая создает таблицу в html
  tableStatistic.innerHTML = "<tr class = 'head'><td><td>1 курс<td>2 курс<td>3 курс<td>4 курс<td>5 курс"; // заполняет шапку таблицы
  body.appendChild(tableStatistic); // Добавляет эту шапку в документ html

  // Создание шапки для таблицы с данными студентов
  let tableHead = document.createElement("table"); // Переменная которая создает таблицу в html
  tableHead.innerHTML = "<tr class = 'head'><td>Удалить<br>студента<td>Имя<td>Оценка<td>Курс<td>Почта<td>Активность<td>Дата добавления"; // заполняет шапку таблицы
  body.appendChild(tableHead); // Добавляет эту шапку в документ html

  // Цикл который заполняет таблицу студентами, а так же в нем происходит прослушивание: чекбоксов на удаление, имя студента, оценок и курса в таблице для создания поля ввода
  for (let i = 0; i < masStudents.length; i++) {

    // Свитч для расчета средней оценки по каждому курсу
    switch (masStudents[i].course) {
      case 1:
        averageScorecourse += masStudents[i].estimate;
        numberOfInputs1++;
        break;
      case 2:
        averageScoreCourse2 += masStudents[i].estimate;
        numberOfInputs2++;
        break;
      case 3:
        averageScoreCourse3 += masStudents[i].estimate;
        numberOfInputs3++;
        break;
      case 4:
        averageScoreCourse4 += masStudents[i].estimate;
        numberOfInputs4++;
        break;
      case 5:
        averageScoreCourse5 += masStudents[i].estimate;
        numberOfInputs5++;
        break;
      default:
    }

    // Свитч для расчета количества неактивных студентов
    if (masStudents[i].active == false) {
      switch (masStudents[i].course) {
        case 1:
          numberOfInactive1++;
          break;
        case 2:
          numberOfInactive2++;
          break;
        case 3:
          numberOfInactive3++;
          break;
        case 4:
          numberOfInactive4++;
          break;
        case 5:
          numberOfInactive5++;
          break;
        default:
      }
    }

    //Расчет студентов добавленных за последний час. Если у студена заполнено время добавления то заходим в if
    let timeNow = Date.now() / 1000 / 60; //Переменная которая хранит в себе текущее время в минутах (кол-во минут с 1970 года)
    let timeStudent = JSON.parse(masStudents[i].time).allTime / 1000 / 60; //Переменная которая хранит в себе время добавления текущего студента в минутах (кол-во минут с 1970 года)
    if (timeNow - timeStudent < 60) { //если разница меньше 60 минут то увеличиваем переменную кол-ва добавленных за час на 1
      studPerHour++;
    }

    //Заполнение таблицы с данными студентов
    let table = document.createElement("tr"); // Переменная которая создает строку в таблице
    //Добавляем класс для новой строки в зависимости от средней оценки студена
    if (masStudents[i].estimate <= 3) {
      table.classList.add("redLine");
    } else if (masStudents[i].estimate > 3 && masStudents[i].estimate <= 4) {
      table.classList.add("yellowLine");
    } else if (masStudents[i].estimate > 4) {
      table.classList.add("greenLine");
    }

    //Добавляем данные студента в таблицу
    table.innerHTML += '<td><input class = "checkboxCheck" type="checkbox" name="a" value=" ' + i + '">' // Заполнение созданной строки данными студентов
    table.innerHTML += '<td class = "nameCheck">' + masStudents[i].name; // Заполнение созданной строки данными студентов
    table.innerHTML += '<td class = "scoreCheck">' + masStudents[i].estimate; // Заполнение созданной строки данными студентов
    table.innerHTML += '<td class = "courseCheck">' + masStudents[i].course; // Заполнение созданной строки данными студентов
    table.innerHTML += '<td class = "emailCheck">' + masStudents[i].email; // Заполнение созданной строки данными студентов
    if (masStudents[i].active == true) { // Добавление дополнительной иконки для измененеия статуса студента
      table.innerHTML += "<td>" + '<img class = "image" src="img/Tick_Mark.svg" alt="альтернативный текст">';
    } else {
      table.innerHTML += "<td>" + '<img class = "image" src="img/73031.svg" alt="альтернативный текст">';
    }
    let time = JSON.parse(masStudents[i].time)
    //Если значение минут меньше 10 - ставим 0 перед числом
    if (time.minutes < 10) {
      time.minutes = "0" + time.minutes;
    }
    if (time.hours < 10) {
      time.hours = "0" + time.hours;
    }
    let month = time.month + 1; //Переменная для приведения месяца в стандарт. JS выдаем на месяца от 0 до 11
    if (month < 10) {
      month = "0" + month;
    }
    table.innerHTML += "<td>" + time.hours + ":" + time.minutes + " " + time.day + "." + month + "." + time.year;
    document.querySelectorAll("tbody")[1].appendChild(table); //Добавление в html созданной строки с заполненными данными

    // Прослушивание чекбоксов с классом checkboxCheck на клик и удаление студена из массива с выбранными студентами
    document.querySelectorAll(".checkboxCheck")[i].addEventListener("click", function() { // Прослушивание чекбоксов на клик
      masStudents.splice(i, 1); // Удаляем из нашего массива студентов один i элемент
      localStorage.setItem("masStudents", JSON.stringify(masStudents));
      body.innerHTML = "";
      outputStudents(); //Перезапускаем нашу функцию, которая заполнит таблицу без удаленного объекта
    });

    //Прослушивание при клике на имя студента и создания поля ввода в этой ячейке вместо имени.
    let nameCheck = document.querySelectorAll(".nameCheck");
    nameCheck[i].addEventListener("click", function() {
      if (nameCheck[i].childNodes[0].tagName == undefined) {
        nameCheck[i].innerHTML = '<input class = "name" type="text"  name="a" value="" style="width:90px;">';
        nameCheck[i].querySelector("input").value = masStudents[i].name;
        nameCheck[i].firstChild.focus();
      }
      nameCheck[i].firstChild.addEventListener("blur", function(event) { // Прослушивание чекбоксов на клик
        nameCheck[i].innerHTML = masStudents[i].name;
      });
    });

    // Прослушивание поля ввода в таблица на нажатие Enter
    nameCheck[i].addEventListener("keydown", function(event) { // Прослушивание чекбоксов на клик
      let testName = /^[A-ZА-Я][a-zа-я]{1,14}$/.test(nameCheck[i].childNodes[0].value);
      if (event.key == "Enter" && testName) {
        masStudents[i].name = nameCheck[i].childNodes[0].value;
        localStorage.setItem("masStudents", JSON.stringify(masStudents));
        body.innerHTML = "";
        outputStudents(); //Перезапускаем нашу функцию, которая заполнит таблицу без удаленного объекта
      } else if (event.key == "Enter") {
        alert("Введите имя с большой буквы используя только буквы")
      }
    });

    //Прослушивание при клике на оценку и создания поля ввода в этой ячейке
    var scoreCheck = document.querySelectorAll(".scoreCheck");
    scoreCheck[i].addEventListener("click", function() {
      if (scoreCheck[i].childNodes[0].tagName == undefined) {
        scoreCheck[i].innerHTML = '<input class = "name" type="text" name="a" value="" style="width:30px;">';
        scoreCheck[i].querySelector("input").value = masStudents[i].estimate;
        scoreCheck[i].firstChild.focus();
      }
      scoreCheck[i].firstChild.addEventListener("blur", function(event) { // Прослушивание чекбоксов на клик
        scoreCheck[i].innerHTML = masStudents[i].estimate;
      });
    });

    // Прослушивание поля ввода оценки в таблице на нажатие Enter
    scoreCheck[i].addEventListener("keydown", function(event) { // Прослушивание чекбоксов на клик
      // let testEstimate = /^[1-5]$/.test(scoreCheck[i].childNodes[0].value); (не работает если вводить дробные числа)
      if (event.key == "Enter" && scoreCheck[i].childNodes[0].value <= 5 && scoreCheck[i].childNodes[0].value > 0) {
        masStudents[i].estimate = Number(scoreCheck[i].childNodes[0].value);
        localStorage.setItem("masStudents", JSON.stringify(masStudents));
        body.innerHTML = "";
        outputStudents(); //Перезапускаем нашу функцию, которая заполнит таблицу без удаленного объекта
      } else if (event.key == "Enter") {
        alert("Введите оценку от 1 до 5")
      }
    });

    //Прослушивание при клике на почту email и создания поля ввода в этой ячейке
    let emailCheck = document.querySelectorAll(".emailCheck");
    let testMail = /^.{1,20}\@{1}[a-z]{1,9}(\.[a-z]{2,4}){1,2}$/.test(emailCheck[i].childNodes[0].value);
    emailCheck[i].addEventListener("click", function() {
      if (emailCheck[i].childNodes[0].tagName == undefined) {
        emailCheck[i].innerHTML = '<input class = "mailTest" type="text"  name="a" style="width:160px;">';
        emailCheck[i].querySelector("input").value = masStudents[i].email;
        emailCheck[i].firstChild.focus();
      }
      emailCheck[i].firstChild.addEventListener("blur", function(event) { // Прослушивание чекбоксов на клик
        emailCheck[i].innerHTML = masStudents[i].email;
      });
    });

    // Прослушивание поля ввода почты в таблице на нажатие Enter
    emailCheck[i].addEventListener("keydown", function(event) { // Прослушивание чекбоксов на клик
      let testMail = /^.{1,20}\@{1}[a-z]{1,9}(\.[a-z]{2,4}){1,2}$/.test(emailCheck[i].childNodes[0].value);
      if (event.key == "Enter" && testMail) {
        masStudents[i].email = emailCheck[i].childNodes[0].value;
        localStorage.setItem("masStudents", JSON.stringify(masStudents));
        body.innerHTML = "";
        outputStudents(); //Перезапускаем нашу функцию, которая заполнит таблицу без удаленного объекта
      } else if (event.key == "Enter") {
        alert("Введите корректно email")
      }
    });

    //Прослушивание при клике на оценку и создания поля ввода в этой ячейке
    let courseCheck = document.querySelectorAll(".courseCheck");
    courseCheck[i].addEventListener("click", function() {
      if (courseCheck[i].childNodes[0].tagName == undefined) {
        courseCheck[i].innerHTML = '<input class = "name" type="text"  name="a" style="width:30px;">';
        courseCheck[i].querySelector("input").value = masStudents[i].course;
        courseCheck[i].firstChild.focus();
      }
      courseCheck[i].firstChild.addEventListener("blur", function(event) { // Прослушивание чекбоксов на клик
        courseCheck[i].innerHTML = masStudents[i].course;
      });
    });

    // Прослушивание поля ввода в таблица на нажатие Enter
    courseCheck[i].addEventListener("keydown", function(event) { // Прослушивание чекбоксов на клик
      let testCourse = /^[1-5]$/.test(courseCheck[i].childNodes[0].value);
      if (event.key == "Enter" && testCourse) {
        masStudents[i].course = Number(courseCheck[i].childNodes[0].value);
        localStorage.setItem("masStudents", JSON.stringify(masStudents));
        body.innerHTML = "";
        outputStudents(); //Перезапускаем нашу функцию, которая заполнит таблицу без удаленного объекта
      } else if (event.key == "Enter") {
        alert("Введите курс от 1 до 5");
      }
    });
    //Прослушиваем все картинки на клик и меняем в массиве активность и обновляем все в html
    document.querySelectorAll(".image")[i].addEventListener("click", function() {
      if (masStudents[i].active == true) {
        masStudents[i].active = false;
      } else {
        masStudents[i].active = true;
      }
      localStorage.setItem("masStudents", JSON.stringify(masStudents));
      body.innerHTML = "";
      outputStudents(); //Перезапускаем нашу функцию, которая заполнит таблицу без удаленного объекта
    });
  }

  //Добавление надписи с количество студентов за последний час
  let hourSrudents = document.createElement("h2");
  hourSrudents.innerHTML = "За последний час добавлено: " + studPerHour + " студенов";
  body.appendChild(hourSrudents);

  // Расчет средней оценки после окончания цикла (в цикле была получена сумма всех оценок и кол-во студентов для вычисление средней оценки)
  averageScorecourse = Math.floor(averageScorecourse / numberOfInputs1 * 100) / 100;
  averageScoreCourse2 = Math.floor(averageScoreCourse2 / numberOfInputs2 * 100) / 100;
  averageScoreCourse3 = Math.floor(averageScoreCourse3 / numberOfInputs3 * 100) / 100;
  averageScoreCourse4 = Math.floor(averageScoreCourse4 / numberOfInputs4 * 100) / 100;
  averageScoreCourse5 = Math.floor(averageScoreCourse5 / numberOfInputs5 * 100) / 100;

  //Заполнение таблицы со средними оценками
  let courseAverage = document.createElement("tr");
  courseAverage.innerHTML += "<td>" + "Ср. оц.";
  courseAverage.innerHTML += "<td>" + averageScorecourse;
  courseAverage.innerHTML += "<td>" + averageScoreCourse2;
  courseAverage.innerHTML += "<td>" + averageScoreCourse3;
  courseAverage.innerHTML += "<td>" + averageScoreCourse4;
  courseAverage.innerHTML += "<td>" + averageScoreCourse5;
  document.querySelectorAll("tbody")[0].appendChild(courseAverage);

  //Заполнение таблицы количество неактивных студентов
  let inactiveStud = document.createElement("tr");
  inactiveStud.innerHTML += "<td>" + "Неакт. студ.";
  inactiveStud.innerHTML += "<td>" + numberOfInactive1;
  inactiveStud.innerHTML += "<td>" + numberOfInactive2;
  inactiveStud.innerHTML += "<td>" + numberOfInactive3;
  inactiveStud.innerHTML += "<td>" + numberOfInactive4;
  inactiveStud.innerHTML += "<td>" + numberOfInactive5;
  document.querySelectorAll("tbody")[0].appendChild(inactiveStud);

  //Цикл в котором перебираем все ячейки в строке со средники оценками по каждому курсу (таблица средних оценок по каждому курсу). Присваиваем ячейке класс в зависимости от оценки
  for (let i = 0; i < courseAverage.childNodes.length; i++) {
    if (courseAverage.childNodes[i].innerHTML <= 3) {
      courseAverage.childNodes[i].classList.add("redLine");
    } else if (courseAverage.childNodes[i].innerHTML > 3 && courseAverage.childNodes[i].innerHTML <= 4) {
      courseAverage.childNodes[i].classList.add("yellowLine");
    } else if (courseAverage.childNodes[i].innerHTML > 4) {
      courseAverage.childNodes[i].classList.add("greenLine");
    }
  }

  // Если массив со студентами стал пустым пишем что студенты не найдены добавляя заголовок h2
  if (masStudents.length == 0) { //если массив студнетов пустой
    let finalText = document.createElement("h2");
    finalText.id = "finalText";
    finalText.innerHTML = "В массиве не осталось студентов";
    body.appendChild(finalText);
  }

  // Прослушивает кнопку добавить на клик и добавляем новых студентов при нажатии как в массив так и в HTML
  document.querySelector("button").addEventListener("click", function() { // Функция которая реагирует на нажатие кнопки

    let table = document.createElement("tr"); // Переменная которая создает строку для таблицы
    let input = document.querySelectorAll("input"); // Переменная которая указывает на все инпуты в документе
    let testMail = /^.{1,20}\@{1}[a-z]{1,9}(\.[a-z]{2,4}){1,2}$/.test(input[4].value); //В начале от 1 до 20 любых символов, потом 1 собака, от 1 до 9 букв от а до z, потом точка и от а до z 2-4 символа. может быть 1 или 2 раза
    let testName = /^[A-ZА-Я][a-zа-я]{1,14}$/.test(input[1].value); // Первая буква большая и еще + 14 маленьких букв (можно было сделать преобразование любого текста в 1 большую и остальные маленькие буквы)
    // let testEstimate = /^[1-5]$/.test(input[2].value); Не подходит так как не принимает дробные значения
    let testCourse = /^[1-5]$/.test(input[3].value);

    if (testMail && testName && input[2].value <= 5 && input[2].value > 0 && testCourse) {

      // Так как после добавления нового стдента в массив у нас идет перезапуск функции в которой есть добавление в HTML студентов из массива. То нам нет
      // смысла добавлять их сразу в HTML код так как он сотрется и будет перезапись

      //Добавление в массив нового студента

      masStudents.push({ //Добавляем в наш массив студентов новый объект с данными указаными в инпутах
        name: input[1].value, //добавление имени в объект нового студента
        estimate: Number(input[2].value), //добавление оценки в объект нового студента
        course: Number(input[3].value), //добавление курса в объект нового студента
        active: document.querySelector(".status").checked, //добавление активности в объект нового студента
        email: input[4].value,
        time: JSON.stringify({
          allTime: Date.now(),
          hours: (new Date).getHours(),
          minutes: (new Date).getMinutes(),
          year: (new Date).getFullYear(),
          month: (new Date).getMonth(),
          day: (new Date).getDate()
        })
      });

      localStorage.setItem("masStudents", JSON.stringify(masStudents));
      //После каждого добавление студентов нужно очистить HTML и внести заново чтобы корректно работал цикл удаления
      body.innerHTML = "";
      outputStudents(); //Перезапускаем нашу функцию, которая заполнит таблицу без удаленного объекта
    } else {
      alert("Заполните корректно все поля");
    }
  });

}
outputStudents(); //Запуск нашей функции
